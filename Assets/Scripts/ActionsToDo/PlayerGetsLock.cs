﻿using UnityEngine;

[CreateAssetMenu(fileName = "PlayerGetsLock", menuName = "ActionsToDo/PlayerGetsLock")]
public class PlayerGetsLock : ActionToDo
{
    public override void ExecuteAction()
    {
        new PlayAudioWhenPlayerGetsLockSignal().Execute();
        new EnableGlassNearLockSignal().Execute();
    }
}