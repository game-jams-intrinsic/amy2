﻿using UnityEngine;

[CreateAssetMenu(fileName = "ChangeToSceneHouse", menuName = "ActionsToDo/ChangeToSceneHouse")]
public class ChangeToSceneHouse : ActionToDo
{
    private bool _actionWasExecutedBefore;

    public override void ExecuteAction()
    {
        if(_actionWasExecutedBefore) return;
        new MuteAllAudiosBeforeExitLevelZeroSignal().Execute();
        new PlayerHasCompletedLevelSignal().Execute();
        _actionWasExecutedBefore = true;
    }
}