﻿using UnityEngine;

[CreateAssetMenu(fileName = "PlayerGetPictureOfHerMother", menuName = "ActionsToDo/PlayerGetPictureOfHerMother")]
public class PlayerGetPictureOfHerMother : ActionToDo
{
    public override void ExecuteAction()
    {
        new PlayAudioAfterTakingMotherPictureSignal().Execute();
    }
}