﻿using UnityEngine;

[CreateAssetMenu(fileName = "VaseHasFallen", menuName = "ActionsToDo/VaseHasFallen")]
public class VaseHasFallen : ActionToDo
{
    [SerializeField] private ActionToDo _actionToDoAfterShake;

    public override void ExecuteAction()
    {
        new PlayVaseHitsGroundSignal().Execute();
        new DisableMouseMovementSignal().Execute();
        new DisablePlayerMovementSignal().Execute();
        new ShakeCameraSignal() {TimeToShake = 4.0f, ActionToDoAfter = _actionToDoAfterShake}.Execute();
    }
}