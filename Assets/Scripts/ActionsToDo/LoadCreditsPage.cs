﻿using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu(fileName = "LoadCreditsPage", menuName = "ActionsToDo/LoadCreditsPage")]
public class LoadCreditsPage : ActionToDo
{
    public override void ExecuteAction()
    {
        new MuteAllAudiosBeforeExitLevelZeroSignal().Execute();
        
        Debug.Log("CREDOTS");
        SceneManager.LoadScene("Credits");
    }
}