﻿using UnityEngine;

[CreateAssetMenu(fileName = "PlayerGetPictureOfTheirDaughters", menuName = "ActionsToDo/PlayerGetPictureOfTheirDaughters")]
public class PlayerGetPictureOfTheirDaughters : ActionToDo
{
    public override void ExecuteAction()
    {
        new PlayAudioAfterTakingDaughtersPictureSignal().Execute();
    }
}