﻿using UnityEngine;

[CreateAssetMenu(fileName = "ShowFlashbackVase", menuName = "ActionsToDo/ShowFlashbackVase")]
public class ShowFlashbackVase : ActionToDo
{
    public override void ExecuteAction()
    {
        new ShowVaseFlashBacksSignal().Execute();
        new PlaySoundFlashBackVaseSignal().Execute();
    }
}