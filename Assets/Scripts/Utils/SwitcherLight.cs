﻿using System.Collections;
using UnityEngine;

namespace Utils
{
    public class SwitcherLight : IEnumerator
    {
        public event System.Action<SwitcherLight> OnCoroutineEnd = delegate { };
        private Light _light;
        private float _timeToSwitch, _destinationIntensity;
        private bool _switchOff;

        public SwitcherLight(Light light, float timeToSwitch, bool switchOff, float destinationIntensity)
        {
            _timeToSwitch = timeToSwitch;
            _light = light;
            _switchOff = switchOff;
            _destinationIntensity = destinationIntensity;
        }

        public IEnumerator Start()
        {
            float counter = 0f;

            //Set Values depending on if fadeIn or fadeOut
            float initLuminosity, destinationLuminosity;

            if (_switchOff)
            {
                initLuminosity = _light.intensity;
                destinationLuminosity = 0;
            }
            else
            {
                initLuminosity = 0;
                destinationLuminosity = _destinationIntensity;
            }


            while (counter < _timeToSwitch)
            {
                counter += Time.deltaTime;
                _light.intensity = Mathf.Lerp(initLuminosity, destinationLuminosity, counter / _timeToSwitch);
                yield return null;
            }

            _light.intensity = destinationLuminosity;
        }

        public bool MoveNext()
        {
            return false;
            // throw new System.NotImplementedException();
        }

        public void Reset()
        {
            // throw new System.NotImplementedException();
        }

        public object Current { get; }
    }
}


namespace Utils
{
}