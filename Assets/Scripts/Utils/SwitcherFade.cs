﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Utils
{
    public class SwitcherFade : IEnumerator
    {
        public event System.Action<SwitcherFade> OnCoroutineEnd = delegate { };
        private Image _image;
        private float _timeToSwitch;
        private bool _fadeOn;
        private Color _color;

        public SwitcherFade(Image image, float timeToSwitch, bool fadeOn)
        {
            _timeToSwitch = timeToSwitch;
            _image = image;
            _fadeOn = fadeOn;
        }

        public IEnumerator Start()
        {
            float counter = 0f;

            float initLuminosity, destinationLuminosity;

            if (_fadeOn)
            {
                initLuminosity = 0;
                destinationLuminosity = 1;
            }
            else
            {
                initLuminosity = 1;
                destinationLuminosity = 0;
            }


            while (counter < _timeToSwitch)
            {
                counter += Time.deltaTime;
                float alpha = Mathf.Lerp(initLuminosity, destinationLuminosity, counter / _timeToSwitch);
                _color = new Color(1, 1, 1, alpha);
                _image.color = _color;
                yield return null;
            }

            _color = new Color(1, 1, 1, destinationLuminosity);
            _image.color = _color;
            OnCoroutineEnd.Invoke(this);
        }

        public bool MoveNext()
        {
            return false;
            // throw new System.NotImplementedException();
        }

        public void Reset()
        {
            // throw new System.NotImplementedException();
        }

        public object Current { get; }
    }
}