﻿using UnityEngine;
using UnityEngine.Assertions;

public class TriggerToSecondZoneLevelZero : MonoBehaviour
{
    [SerializeField] private ColliderPassingChecker _colliderPassingChecker;
    [SerializeField] private Transform _positionToResetPlayer;
    [SerializeField] private Transform _playerTransform;

    private bool _hasCrossedOnce = false;

    private void Start()
    {
#if UNITY_EDITOR
        Assert.IsNotNull(_playerTransform);
#endif
        _colliderPassingChecker.OnPlayerPassCollider += PlayerPassCollider;
    }

    private void PlayerPassCollider()
    {
        if (!_hasCrossedOnce)
        {
            new StartSwitcherFirstLightlevelZeroSignal().Execute();
            new StartSwitcherSecondLightSignal().Execute();
            new PlayPlayerReachGreyZoneSoundSignal().Execute();
            _hasCrossedOnce = true;
        }
        else
        {
            _playerTransform.position = _positionToResetPlayer.position;
        }
    }
}