﻿using Signals.EventManager;
using UnityEngine;
using Utils;

public class AudioLevelZeroSignalManager : MonoBehaviour
{
    [SerializeField] private AudioToPlay _audiosToPlay;

    [SerializeField] private AudioSource
        _audioSourceBackgroundSoundLevelZero;

    private IEventManager _eventManager;

    void Awake()
    {
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
    }

    private void Start()
    {

        _eventManager.AddActionToSignal<PlayPlayerGoToGreyZoneSoundSignal>(PlayPlayerGoToGreyZoneSound);
        _eventManager.AddActionToSignal<PlayPlayerReachGreyZoneSoundSignal>(PlayPlayerReachGreyZoneSound);
        _eventManager.AddActionToSignal<MuteAllAudiosBeforeExitLevelZeroSignal>(MuteAllAudios);
        _eventManager.AddActionToSignal<PlayPlayerReachCenterSoundSignal>(PlayPlayerReachCenterSound);
        _eventManager.AddActionToSignal<PlayWhenTriggerLockedDoorSoundSignal>(PlayWhenTriggerLockedDoorSound);
        _audioSourceBackgroundSoundLevelZero.Play();
    }

    private void PlayWhenTriggerLockedDoorSound(PlayWhenTriggerLockedDoorSoundSignal obj)
    {
        _audiosToPlay.GetAudio("WhenTriggerLockedDoor",
            ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageKey()).Play();
    }

    private void PlayPlayerReachGreyZoneSound(PlayPlayerReachGreyZoneSoundSignal obj)
    {
        _audiosToPlay.GetAudio("WhenPlayerReachToGreyZone",
            ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageKey()).Play();
    }

    private void PlayPlayerReachCenterSound(PlayPlayerReachCenterSoundSignal obj)
    {
        _audiosToPlay.GetAudio("WhenPlayerReachCenterZoneAfterGoThroughDoor",
            ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageKey()).Play();
    }

    private void MuteAllAudios(MuteAllAudiosBeforeExitLevelZeroSignal obj)
    {
        _audioSourceBackgroundSoundLevelZero.Stop();
    }

    private void PlayPlayerGoToGreyZoneSound(PlayPlayerGoToGreyZoneSoundSignal obj)
    {
        _audiosToPlay.GetAudio("WhenPlayerGoToGreyLightSound",
            ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageKey()).Play();
    }
}