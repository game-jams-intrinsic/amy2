﻿using System;
using System.Collections;
using System.Collections.Generic;
using Signals.EventManager;
using UnityEngine;
using Utils;

public class DoorManagerLevelLimbo : MonoBehaviour
{
    [SerializeField] private GameObject _doorToActivate;

    private IEventManager _eventManager;

    private void Awake()
    {
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
    }

    void Start()
    {
        _doorToActivate.gameObject.SetActive(false);

        _eventManager.AddActionToSignal<StartSwitcherSecondLightSignal>(EnableDoor);
    }

    private void OnDestroy()
    {
        _eventManager.RemoveActionFromSignal<StartSwitcherSecondLightSignal>(EnableDoor);
    }

    private void EnableDoor(StartSwitcherSecondLightSignal obj)
    {
        _doorToActivate.gameObject.SetActive(true);
    }
}