﻿using UnityEngine;
using Utils;
using Utils.Input;

public class CanvasInitLevelPresenterLevelZero : MonoBehaviour
{
    [SerializeField] private CanvasGroup _canvasGroupWasd;
    [SerializeField] private TextWriter textWriter;
    private ReadInputPlayer _readInputPlayer;
    private bool _isHidden = false;
    private void Awake()
    {
        _readInputPlayer = ServiceLocator.Instance.GetService<ReadInputPlayer>();
        Time.timeScale = 1;

    }

    void Start()
    {
        _canvasGroupWasd.gameObject.SetActive(false);
        // _readInputPlayer.OnDebugEscPressed += PassInitText;
        _readInputPlayer.OnPlayerStartMoving += HideWASDImage;
        textWriter.OnTextHasEnd += InitialTextHasEnded;
    }
    
    private void OnDestroy()
    {
        // _readInputPlayer.OnDebugEscPressed -= PassInitText;
        textWriter.OnTextHasEnd -= InitialTextHasEnded;
        _readInputPlayer.OnPlayerStartMoving -= HideWASDImage;

    }

    private void InitialTextHasEnded()
    {
        ShowWASDImage();
        EnablePlayerMovement();
        EnableFirstLight();
        new EnableMovementAfterEventEndedSignal().Execute();
        new ResumeRotationCameraSignal().Execute();
    }

    private void EnableFirstLight()
    {
        new EnableFirstLightSignal().Execute();
    }

    private void HideWASDImage()
    {
        if (_isHidden)
        {
            return;
        }

        _canvasGroupWasd.gameObject.SetActive(false);
        new PlayPlayerGoToGreyZoneSoundSignal().Execute();
        _isHidden = true;
    }

    private void PassInitText()
    {
        if (!textWriter.ShowingText) return;
        textWriter.TextHasEnd();
        textWriter.gameObject.SetActive(false);
    }

    private void ShowWASDImage()
    {
        _canvasGroupWasd.gameObject.SetActive(true);
    }

    private void EnablePlayerMovement()
    {
        new EnablePlayerMovementSignal().Execute();
    }
}