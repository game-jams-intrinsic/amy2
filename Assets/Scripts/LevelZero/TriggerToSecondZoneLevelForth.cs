﻿using UnityEngine;

public class TriggerToSecondZoneLevelForth : MonoBehaviour
{
    [SerializeField] private ColliderPassingCheckerV2 _colliderPassingChecker;

    private bool _hasCrossedOnce = false;

    private void Start()
    {
        _colliderPassingChecker.OnPlayerPassCollider += PlayerPassCollider;
    }

    private void PlayerPassCollider()
    {
        if (_hasCrossedOnce) return;
        new StartSwitcherFirstLightlevelZeroSignal().Execute();
        new StartSwitcherSecondLightSignal().Execute();
        _hasCrossedOnce = true;
    }
}