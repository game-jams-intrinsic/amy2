﻿using System.Collections;
using System.Collections.Generic;
using Signals.EventManager;
using UnityEngine;
using Utils;

public class MirrorShutdown : MonoBehaviour
{
    [SerializeField] private List<GameObject> _mirrors;

    private IEventManager _eventManager;
    // Start is called before the first frame update
    void Start()
    {
        _eventManager =   ServiceLocator.Instance.GetService<IEventManager>();
        _eventManager.AddActionToSignal<DisableMirrorsLimboSignal>(DisableMirrorsLimbo);
    }

    private void DisableMirrorsLimbo(DisableMirrorsLimboSignal signal)
    {
        foreach (var VARIABLE in _mirrors)
        {
            VARIABLE.gameObject.SetActive(false);
        }
    }
}