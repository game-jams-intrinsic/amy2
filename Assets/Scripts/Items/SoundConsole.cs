﻿using Presentation.Items;
using UnityEngine;
using Utils;

public class SoundConsole : InteractableItem
{
    private IConsolePanelModel _consoleModel;
    private bool _hasBeenTouchedBefore;

    private void Start()
    {
        _consoleModel = ServiceLocator.Instance.GetModel<IConsolePanelModel>();
        SetTextToShow(ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageText()
            .TextToGetTouchSoundConsoleLevelTwoInGameKey);
    }

    public override void InteractWithObject()
    {
        _hasBeenTouchedBefore = true;
        new PlayAudioWhenPlayerTouchesConsoleButtonSignal().Execute();
        _consoleModel.ConsoleIsTouched = true;
        gameObject.layer = LayerMask.NameToLayer("Default");
    }

    public override void InteractWithObject(GameObject objectWhichInteract)
    {
        InteractWithObject();
    }
}
