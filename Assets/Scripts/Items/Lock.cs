﻿using System.Collections;
using System.Collections.Generic;
using Presentation.Items;
using UnityEngine;
using Utils;

public class Lock : InteractableItem
{
    [SerializeField] private Sprite _sprite;

    private bool _hasBeenTouchedBefore;
    [SerializeField] private ActionToDo _actionToDo;

    private void Start()
    {
        _hasBeenTouchedBefore = false;
        SetTextToShow(ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageText().TextToGetGetLockInGameKey);
    }
    

    public override void InteractWithObject()
    {
        new ShowPictureOnCanvasSignal() {PictureToShow = _sprite}.Execute();
        if (_hasBeenTouchedBefore || !_actionToDo )
        {
            return;
        }

        _hasBeenTouchedBefore = true;
        _actionToDo.ExecuteAction();
    }

    public override void InteractWithObject(GameObject objectWhichInteract)
    {
        InteractWithObject();
    }
}