﻿using UnityEngine;
using Utils;

public class DoorLockedByKeyCard : DoorWithButton
{
    
    private void Start()
    {
        _colliderPassingChecker.OnPlayerPassCollider += InteractWith;
        buttonToOpenDoor.OnPlayerInteractWithButton += InteractWith;
        if (_isOpen)
        {
            OpenDoor();
        }
    }

    private void OnDestroy()
    {
        base.OnDestroy();
        buttonToOpenDoor.OnPlayerInteractWithButton -= InteractWith;
        _colliderPassingChecker.OnPlayerPassCollider -= InteractWith;
    }

    public void SetButtonColourToRed()
    {
        _buttonRenderer1.material = _redColour;
    }

    public void SetButtonColourToOriginal()
    {
        _buttonRenderer1.material = _originalColour;
    }

    public void ShowTextToAdvisePlayerToGetKeyCard()
    {
        new ShowTextOnCanvasSignal()
        {
            TextToShow = ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageText()
                .TextPlayerNeedToGetKeyCardInGameKey,
            Color = Color.white
        }.Execute();
    }
}