﻿using System.Collections.Generic;
using System.Linq;

public class GlassPieceManagerLevelTwoInsideConservatory : GlassPieceManager
{
    public List<GlassInitInfo> _glassPieces;

    public override List<GlassInitInfo> GlassPieces()
    {
        return _glassPieces;
    }
    
    void Start()
    {
        _glassPiecesRecovered = 0;
        _eventManager.AddActionToSignal<PlayerPutsGlassPieceOnMirrorSignal>(PlayerHasPutGlassPieceOnMirror);
        _eventManager.AddActionToSignal<EnableGlassAfterPlayPianoSignal>(EnableGlassAfterPlayPiano);
        foreach (var glassPiece in _glassPieces)
        {
            if (glassPiece.IsEnabledAtFirst)
            {
                _idGlassEnabledAtFirst = glassPiece.GlassPiece.PieceId;
            }

            glassPiece.GlassPiece.gameObject.SetActive(glassPiece.IsEnabledAtFirst);
        }
        
    }

    private void EnableGlassAfterPlayPiano(EnableGlassAfterPlayPianoSignal obj)
    {
        GlassInitInfo glassPiece = _glassPieces.Single(x => x.Localization == "Piano");
        glassPiece.GlassPiece.gameObject.SetActive(true);
    }

    protected override void PlayerHasPutGlassPieceOnMirror(PlayerPutsGlassPieceOnMirrorSignal obj)
    {

        base.PlayerHasPutGlassPieceOnMirror(null);

        if (_glassPiecesRecovered != _glassPieces.Count) return;
        new PlayAudioWhenMirrorInsideConservatoryIsCompletedSignal().Execute();
        new ShowFlashBackWhenMirrorInsideConservatoryIsCompletedLevelTwoSignal().Execute();
        new SwitchOffAllLightsLevelTwoSignal().Execute();
        Destroy(gameObject);
    }
}