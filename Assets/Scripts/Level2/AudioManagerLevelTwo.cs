﻿using System;
using Signals.EventManager;
using UnityEngine;
using Utils;

public class AudioManagerLevelTwo : MonoBehaviour
{
    [SerializeField] private AudioToPlay _audios;
    [SerializeField] private AudioSource _audioSourceBackGround;
    [SerializeField] private AudioSource _audioSourceWhenPlayerPutGlassPieceInMirror;
    [SerializeField] private AudioSource _audioSourceStreetLevelBackGround;
    
    // [SerializeField] private AudioSource _audioSourceWhenPlayerGoToConservatorySound;
    // [SerializeField] private AudioSource _audioSourceWhenPlayerGoToChildrenParkAudioFirst;

    // [SerializeField] private AudioSource _audioSourceWhenPlayerGoToChildrenParkAudioSecond;
    // [SerializeField] private AudioSource _audioSourceWhenPlayerGoToPianoForFirstTimeSound;
    // [SerializeField] private AudioSource _audioSourceInitialAudioWhenLevelStartsAudio;
    // [SerializeField] private AudioSource _audioSourceWhenPlayerIsInFrontOfMirrorForFirstTimeAudio;

    // [SerializeField] private AudioSource _audioSourcePlayWhenPlayerGoToEntranceInside;
    // [SerializeField] private AudioSource _audioSourceWhenPlayerTakesDaughtersPicture;
    // [SerializeField] private AudioSource _audioSourceWhenPlayerTouchesConsoleSound;
    // [SerializeField] private AudioSource _audioSourceWhenPlayerTouchesPianoAndNotRecording;
    // [SerializeField] private AudioSource _audioSourceWhenPlayerTouchesPianoAndIsRecording;
    // [SerializeField] private AudioSource _audioSourceWhenPlayerEntersRoomConservatory;
    // [SerializeField] private AudioSource _audioSourceWhenPlayerPlaysOutsidePiano;
    // [SerializeField] private AudioSource _audioSourceWhenPianoInsideSoundEndedd;
    // [SerializeField] private AudioSource _audioSourceWhenMirrorInsideConservatoryIsCompleted;
    // [SerializeField] private AudioSource _whenPlayerGoTroughOutsideMirrorToConservatory;
    // [SerializeField] private AudioSource _audioSourceWhenPlayerTriggerLockedDoorSound;

    [SerializeField] float _timeToPlayVoiceAfterPlayPiano;

    private IEventManager _eventManager;
    private Timer _timer, _timer2;

    private bool _firstAudioIsPlaying;
    private bool _voiceAfterPianoHasBeenPlayed;
    private bool _pianoSoundIsBeenPlaying;
    private bool _pianoOutsideSoundIsBeenPlaying;

    private void Awake()
    {
       

        _timer = new Timer();
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
        
        _eventManager.AddActionToSignal<PlayChildrenParkConversationSignal>(PlayFirstChildrenParkConversation);
        _eventManager.AddActionToSignal<PlayConservatoryConversationSignal>(PlayConservatoryConversation);
        _eventManager.AddActionToSignal<PlayWhenPlayerGoToPianoConversationSignal>(PlayWhenPlayerGoToPianoConversation);
        _eventManager.AddActionToSignal<PlayWhenPlayerIsInFrontOfMirrorLevelTwoSignal>(PlayWhenPlayerIsInFrontOfMirror);
        _eventManager.AddActionToSignal<PlayGlassPiecesIsPutInMirrorAudioSignal>(PlayGlassPiecesIsPutInMirrorAudio);
        _eventManager.AddActionToSignal<PlayWhenPlayerGoToEntranceInsideSignal>(
            PlayWhenPlayerGoToEntranceInside);
        _eventManager.AddActionToSignal<PlayAudioAfterTakingDaughtersPictureSignal>(
            PlayAudioAfterTakingDaughtersPicture);
        _eventManager.AddActionToSignal<PlayAudioWhenMirrorInsideConservatoryIsCompletedSignal>(
            PlayAudioWhenMirrorInsideConservatoryIsCompleted);
        _eventManager.AddActionToSignal<PlayAudioWhenPlayerTouchesPianoAndIsRecordingSignal>(
            PlayAudioWhenPlayerTouchesAndIsRecordingPiano);
        _eventManager.AddActionToSignal<PlayAudioWhenPlayerTouchesPianoAndIsNotRecordingSignal>(
            PlayAudioWhenPlayerTouchesPianoAndIsNotRecording);
        _eventManager.AddActionToSignal<PlayAudioWhenPlayerTouchesConsoleButtonSignal>(
            PlayAudioWhenPlayerTouchesConsoleButton);
        _eventManager.AddActionToSignal<PlayAudioWhenPlayerEntersRoomConservatorySignal>(
            PlayAudioWhenPlayerEntersRoomConservatory);
        _eventManager.AddActionToSignal<WhenPlayePlayPianoOutsideSignal>(
            WhenPlayerPlayPianoOutside);
        _eventManager.AddActionToSignal<PlayWhenPlayerGoTroughOutsideMirrorToConservatorySignal>(
            PlayWhenPlayerAppearsInConservatory);
        _eventManager.AddActionToSignal<PlayWhenTriggerLockedDoorSoundSignal>(PlayWhenTriggerLockedDoorSound);
    }


    private void OnDestroy()
    {
        _eventManager.RemoveActionFromSignal<PlayChildrenParkConversationSignal>(PlayFirstChildrenParkConversation);
        _eventManager.RemoveActionFromSignal<PlayChildrenParkConversationSignal>(PlayFirstChildrenParkConversation);
        _eventManager.RemoveActionFromSignal<PlayConservatoryConversationSignal>(PlayConservatoryConversation);
        _eventManager.RemoveActionFromSignal<PlayWhenPlayerGoToPianoConversationSignal>(
            PlayWhenPlayerGoToPianoConversation);
        _eventManager.RemoveActionFromSignal<PlayWhenPlayerIsInFrontOfMirrorLevelTwoSignal>(
            PlayWhenPlayerIsInFrontOfMirror);
        _eventManager
            .RemoveActionFromSignal<PlayGlassPiecesIsPutInMirrorAudioSignal>(PlayGlassPiecesIsPutInMirrorAudio);
        _eventManager.RemoveActionFromSignal<PlayWhenPlayerGoToEntranceInsideSignal>(
            PlayWhenPlayerGoToEntranceInside);
        _eventManager.RemoveActionFromSignal<PlayAudioAfterTakingDaughtersPictureSignal>(
            PlayAudioAfterTakingDaughtersPicture);
        _eventManager.RemoveActionFromSignal<PlayAudioWhenPlayerTouchesPianoAndIsRecordingSignal>(
            PlayAudioWhenPlayerTouchesAndIsRecordingPiano);
        _eventManager.RemoveActionFromSignal<PlayAudioWhenPlayerTouchesPianoAndIsNotRecordingSignal>(
            PlayAudioWhenPlayerTouchesPianoAndIsNotRecording);
        _eventManager.RemoveActionFromSignal<PlayAudioWhenPlayerTouchesConsoleButtonSignal>(
            PlayAudioWhenPlayerTouchesConsoleButton);
        _eventManager.RemoveActionFromSignal<PlayAudioWhenPlayerEntersRoomConservatorySignal>(
            PlayAudioWhenPlayerEntersRoomConservatory);
        _eventManager.RemoveActionFromSignal<PlayAudioWhenMirrorInsideConservatoryIsCompletedSignal>(
            PlayAudioWhenMirrorInsideConservatoryIsCompleted);
        _eventManager.RemoveActionFromSignal<WhenPlayePlayPianoOutsideSignal>(
            WhenPlayerPlayPianoOutside);
        _eventManager.RemoveActionFromSignal<PlayWhenTriggerLockedDoorSoundSignal>(PlayWhenTriggerLockedDoorSound);
    }

    private void Start()
    {
        _audioSourceStreetLevelBackGround.Play();
        _audioSourceBackGround.Play();
        _audios.GetAudio("InitialAudioWhenLevelStarts",
            ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageKey()).Play();
    }

    private void PlayWhenTriggerLockedDoorSound(PlayWhenTriggerLockedDoorSoundSignal obj)
    {
        var audioToPlay = _audios.GetAudio("WhenPlayerTriggersLockedDoorSound",
            ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageKey());
        if (audioToPlay.isPlaying)
        {
            return;
        }

        audioToPlay.Play();
    }


    private void WhenPlayerPlayPianoOutside(WhenPlayePlayPianoOutsideSignal obj)
    {
        if (_pianoOutsideSoundIsBeenPlaying)
        {
            return;
        }

        var audioToPlay = _audios.GetAudio("WhenPlayerPlaysOutsidePiano",
            ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageKey());
        audioToPlay.Play();
        _pianoOutsideSoundIsBeenPlaying = true;
        _timer = new Timer();
        _timer.SetTimeToWait(audioToPlay.clip.length);
        _timer.OnTimerEnds += AudioPianoOutsideHasEnded;
        StartCoroutine(_timer.TimerCoroutine());
    }

    private void AudioPianoOutsideHasEnded()
    {
        _timer.OnTimerEnds -= AudioPianoOutsideHasEnded;
        _pianoOutsideSoundIsBeenPlaying = false;
    }

    private void PlayWhenPlayerAppearsInConservatory(
        PlayWhenPlayerGoTroughOutsideMirrorToConservatorySignal obj)
    {
        _audios.GetAudio("WhenPlayerAppearsInConservatory",
            ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageKey()).Play();
    }

    private void PlayAudioWhenMirrorInsideConservatoryIsCompleted(
        PlayAudioWhenMirrorInsideConservatoryIsCompletedSignal obj)
    {
        var audioToPlay = _audios.GetAudio("WhenMirrorInsideConservatoryIsCompleted",
            ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageKey());
        audioToPlay.Play();
        _timer = new Timer();
        _timer.SetTimeToWait(audioToPlay.clip.length);
        _timer.OnTimerEnds += AudioEndLevelTwoAfterCompleteMirrorInsideConservatoryHasEnded;
        StartCoroutine(_timer.TimerCoroutine());
    }

    private void AudioEndLevelTwoAfterCompleteMirrorInsideConservatoryHasEnded()
    {
        _timer.OnTimerEnds -= AudioEndLevelTwoAfterCompleteMirrorInsideConservatoryHasEnded;

        new AudioEndLevelTwoAfterCompleteMirrorInsideConservatoryHasEndedSignal().Execute();
    }

    private void PlayAudioWhenPlayerTouchesPianoAndIsNotRecording(
        PlayAudioWhenPlayerTouchesPianoAndIsNotRecordingSignal obj)
    {
        _audios.GetAudio("WhenPlayerTouchesPianoAndNotRecording",
            ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageKey()).Play();
    }


    private void PlayAudioWhenPlayerTouchesAndIsRecordingPiano(PlayAudioWhenPlayerTouchesPianoAndIsRecordingSignal obj)
    {
        if (_pianoSoundIsBeenPlaying)
        {
            return;
        }

        var audioToPlay = _audios.GetAudio("WhenPlayerTouchesPianoAndIsRecording",
            ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageKey());
        audioToPlay.Play();
        _pianoSoundIsBeenPlaying = true;
        _timer = new Timer();
        _timer.SetTimeToWait(_timeToPlayVoiceAfterPlayPiano);
        _timer.OnTimerEnds += PlayVoiceAfterPlayerPlaysPianoInside;
        StartCoroutine(_timer.TimerCoroutine());

        _timer2 = new Timer();
        _timer2.SetTimeToWait(audioToPlay.clip.length);
        _timer2.OnTimerEnds += EnableTouchPianoAgain;
        StartCoroutine(_timer2.TimerCoroutine());
    }

    private void EnableTouchPianoAgain()
    {
        _timer2.OnTimerEnds -= EnableTouchPianoAgain;
        _pianoSoundIsBeenPlaying = false;
    }

    private void PlayVoiceAfterPlayerPlaysPianoInside()
    {
        _timer.OnTimerEnds -= PlayVoiceAfterPlayerPlaysPianoInside;

        if (_voiceAfterPianoHasBeenPlayed)
        {
            return;
        }

        new EnableGlassAfterPlayPianoSignal().Execute();

        _voiceAfterPianoHasBeenPlayed = true;
        _audios.GetAudio("WhenSoundPianoInsideEnds",
            ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageKey()).Play();
    }

    private void PlayAudioWhenPlayerEntersRoomConservatory(PlayAudioWhenPlayerEntersRoomConservatorySignal obj)
    {
        _audios.GetAudio("WhenPlayerEntersRoomConservatory",
            ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageKey()).Play();
    }

    private void PlayAudioWhenPlayerTouchesConsoleButton(PlayAudioWhenPlayerTouchesConsoleButtonSignal obj)
    {
        _audios.GetAudio("WhenPlayerTouchesConsole",
            ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageKey()).Play();
    }

    private void PlayAudioAfterTakingDaughtersPicture(PlayAudioAfterTakingDaughtersPictureSignal obj)
    {
        _audios.GetAudio("WhenPlayerTakesDaughtersPicture",
            ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageKey()).Play();
    }

    private void PlayWhenPlayerGoToEntranceInside(PlayWhenPlayerGoToEntranceInsideSignal obj)
    {
        _audios.GetAudio("WhenPlayerGoToEntranceInside",
            ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageKey()).Play();
    }


    private void PlayGlassPiecesIsPutInMirrorAudio(PlayGlassPiecesIsPutInMirrorAudioSignal obj)
    {
        _audioSourceWhenPlayerPutGlassPieceInMirror.Play();
    }


    private void PlayWhenPlayerIsInFrontOfMirror(PlayWhenPlayerIsInFrontOfMirrorLevelTwoSignal obj)
    {
        _audios.GetAudio("WhenPlayerIsInFrontOfMirrorForFirstTime",
            ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageKey()).Play();
    }

    private void PlayFirstChildrenParkConversation(PlayChildrenParkConversationSignal obj)
    {
        var audioToPlay = _audios.GetAudio("WhenPlayerGoToChildrenParkAudioFirst",
            ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageKey());

        audioToPlay.Play();
        _timer = new Timer();
        _timer.OnTimerEnds += PlaySecondChildrenParkConversation;

        _timer.SetTimeToWait(audioToPlay.clip.length);
        StartCoroutine(_timer.TimerCoroutine());
    }

    private void PlaySecondChildrenParkConversation()
    {
        var audioToPlay = _audios.GetAudio("WhenPlayerGoToChildrenParkAudioSecond",
            ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageKey());

        _timer.OnTimerEnds -= PlaySecondChildrenParkConversation;
        audioToPlay.Play();
    }


    private void PlayWhenPlayerGoToPianoConversation(PlayWhenPlayerGoToPianoConversationSignal obj)
    {
        _audios.GetAudio("WhenPlayerGoToPianoForFirstTime",
            ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageKey()).Play();
    }

    private void PlayConservatoryConversation(PlayConservatoryConversationSignal obj)
    {
        _audios.GetAudio("WhenPlayerGoToConservatory",
            ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageKey()).Play();

        // _audioSourceWhenPlayerGoToConservatorySound.Play();
    }
}