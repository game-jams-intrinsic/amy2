﻿using Signals.EventManager;
using UnityEngine;

public class ZoneTriggerConservatory : MonoBehaviour
{
    [SerializeField] private ColliderTriggerZone _colliderTriggerZone;
    private IEventManager _eventManager;
    private bool _hasEnabledBefore;
    private void Start()
    {
        _colliderTriggerZone.OnPlayerTouchCollider += PlayZoneAudio;
    }

    private void OnDestroy()
    {
        _colliderTriggerZone.OnPlayerTouchCollider -= PlayZoneAudio;
    }

    private void PlayZoneAudio(Transform obj)
    {
        if (_hasEnabledBefore)
        {
            return;
        }

        _hasEnabledBefore = true;
        new PlayConservatoryConversationSignal().Execute();
        Destroy(gameObject);
    }
}