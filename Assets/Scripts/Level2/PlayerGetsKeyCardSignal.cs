﻿using Signals;
using UnityEngine;

public class PlayerGetsKeyCardSignal : SignalBase
{
    public Sprite SpriteToShow;
}