﻿using UnityEngine;
using Utils;

[CreateAssetMenu(fileName = "PlayerTouchesPianoInside", menuName = "ActionsToDo/PlayerTouchesPianoInsideAction")]

public class PlayerTouchesPianoInsideAction : ActionToDo
{
    public override void ExecuteAction()
    {
        if (ServiceLocator.Instance.GetModel<IConsolePanelModel>().ConsoleIsTouched)
        {
            new PlayAudioWhenPlayerTouchesPianoAndIsRecordingSignal().Execute();
            return;
        }

        new PlayAudioWhenPlayerTouchesPianoAndIsNotRecordingSignal().Execute();
    }
}