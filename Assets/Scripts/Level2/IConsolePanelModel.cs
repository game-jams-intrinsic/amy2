﻿public interface IConsolePanelModel
{
    bool ConsoleIsTouched { get; set; }
}