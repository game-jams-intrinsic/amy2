﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Utils;

[Serializable]
public class AudioToPlay
{
    [Serializable]
    public struct AudioForLanguage
    {
        public LanguagesKeys LanguagesKeys;
        public AudioSource AudioSource;
    }

    [Serializable]
    public struct AudioForKey
    {
        public String AudioKey;
        public List<AudioForLanguage> AudioForLanguage;
    }

    public List<AudioForKey> Audio;

    public AudioSource GetAudio(String audioKey, LanguagesKeys languagesKeys)
    {
        return Audio.Single(X => X.AudioKey == audioKey).AudioForLanguage.Single(x => x.LanguagesKeys == languagesKeys)
            .AudioSource;
    }

    public AudioSource GetAudioOfCurrentLanguage(String audioKey)
    {
        return Audio.Single(X => X.AudioKey == audioKey).AudioForLanguage.Single(x =>
                x.LanguagesKeys == ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageKey())
            .AudioSource;
    }
}