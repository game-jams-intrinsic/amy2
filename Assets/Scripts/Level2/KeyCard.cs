﻿using App.PlayerModelInfo;
using Presentation.Items;
using UnityEngine;
using Utils;

public class KeyCard : PickableItem
{
    [SerializeField] private Sprite _spriteToShow;

    private void Start()
    {
        SetTextToShow(ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageText()
            .TextToGetKeyCardInGameKey);
    }

    public override void InteractWithObject()
    {
        InteractWithObject();
    }

    public override void InteractWithObject(GameObject objectWhichInteract)
    {
        ServiceLocator.Instance.GetModel<IPlayerModel>().HasKeyCard = true;
        new PlayerGetsKeyCardSignal {SpriteToShow = _spriteToShow}.Execute();
        HideMessageOverItem();
        Destroy(gameObject);
    }
}