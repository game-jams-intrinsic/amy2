﻿using Presentation.Items;
using UnityEngine;
using Utils;

public class Piano : InteractableItem
{
    [SerializeField] private ActionToDo _actionToDo;

    private void Start()
    {
        SetTextToShow(ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageText()
            .TextToGetTouchPianoInGameKey);
    }

    public override void InteractWithObject()
    {
        Debug.Log($"Piano {gameObject.name}");
        _actionToDo.ExecuteAction();
    }

    public override void InteractWithObject(GameObject objectWhichInteract)
    {
        InteractWithObject();
    }
}