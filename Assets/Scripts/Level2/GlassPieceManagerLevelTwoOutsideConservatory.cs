﻿using System.Collections.Generic;
using System.Linq;

public class GlassPieceManagerLevelTwoOutsideConservatory : GlassPieceManager
{
    public List<GlassInitInfo> _glassPieces;

    public override List<GlassInitInfo> GlassPieces()
    {
        return _glassPieces;
    }

    void Start()
    {
        _glassPiecesRecovered = 0;
        _eventManager.AddActionToSignal<PlayerPutsGlassPieceOnMirrorSignal>(PlayerHasPutGlassPieceOnMirror);
        _eventManager.AddActionToSignal<EnableGlassNearLockSignal>(EnableGlassNearLock);
        foreach (var glassPiece in _glassPieces)
        {
            if (glassPiece.IsEnabledAtFirst)
            {
                _idGlassEnabledAtFirst = glassPiece.GlassPiece.PieceId;
            }

            glassPiece.GlassPiece.gameObject.SetActive(glassPiece.IsEnabledAtFirst);
        }
    }

    private void EnableGlassNearLock(EnableGlassNearLockSignal obj)
    {
        GlassInitInfo glassPiece = _glassPieces.Single(x => x.Localization == "Lock");
        glassPiece.GlassPiece.gameObject.SetActive(true);
    }

    protected override void PlayerHasPutGlassPieceOnMirror(PlayerPutsGlassPieceOnMirrorSignal obj)
    {
        base.PlayerHasPutGlassPieceOnMirror(null);

        if (_glassPiecesRecovered != _glassPieces.Count) return;
        new DisablePlayerMovementSignal().Execute();

        new StartFadeSignal().Execute();
        Destroy(gameObject);
    }
}