﻿using UnityEngine;

[CreateAssetMenu(fileName = "PlayerTouchesPianoOutside", menuName = "ActionsToDo/PlayerTouchesPianoOutside")]
public class PlayerTouchesPianoOutsideAction : ActionToDo
{
    public override void ExecuteAction()
    {
        new WhenPlayePlayPianoOutsideSignal().Execute();
    }
}