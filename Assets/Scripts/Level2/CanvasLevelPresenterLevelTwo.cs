﻿using System.Linq;
using App.PlayerModelInfo;
using UnityEngine;
using UnityEngine.UI;
using Utils;

public class CanvasLevelPresenterLevelTwo : CanvasLevelPresenter
{
    [SerializeField] private GlassPieceManager _glassPieceManagerInside, _glassPieceManagerInsideOutside;
    private GlassPieceManager _actualGlassPieceManager;
    private FlashBackViewLevelTwo _flashBackViewLevelTwo;
    [SerializeField] private Fader _fader;
    [SerializeField] private Image _KeyCard;

    private void Awake()
    {
        base.Awake();
        _flashBackViewLevelTwo = _flashBackViewLevel.GetComponent<FlashBackViewLevelTwo>();
        _pieceImage.gameObject.SetActive(false);
        _KeyCard.gameObject.SetActive(false);
    }

    private void Start()
    {
        base.Start();
        _fader.OnChangePieceGlassManager += ChangeGlassPieceManagerManagerToInside;
        _actualGlassPieceManager = _glassPieceManagerInsideOutside;
        _eventManager.AddActionToSignal<ShowFlashBackWhenMirrorInsideConservatoryIsCompletedLevelTwoSignal>(
            ShowFlashBackWhenMirrorIsCompleted);
        _eventManager.AddActionToSignal<ShowPictureOnCanvasSignal>(
            HideKeyCard);
        _eventManager.AddActionToSignal<PlayerGetsKeyCardSignal>(
            PlayerGetsKeyCard);
    }

    private void HideKeyCard(ShowPictureOnCanvasSignal obj)
    {
        HideKeyCard();
    }

    private void PlayerGetsKeyCard(PlayerGetsKeyCardSignal obj)
    {
        ShowKeyCard();
        _KeyCard.sprite = obj.SpriteToShow;
    }

    private void OnDestroy()
    {
        base.OnDestroy();
        _eventManager.RemoveActionFromSignal<ShowFlashBackWhenMirrorInsideConservatoryIsCompletedLevelTwoSignal>(
            ShowFlashBackWhenMirrorIsCompleted);
        _fader.OnChangePieceGlassManager -= ChangeGlassPieceManagerManagerToInside;
    }


    private void ChangeGlassPieceManagerManagerToInside()
    {
        _actualGlassPieceManager = _glassPieceManagerInside;
    }

    protected override void ShowPiece(PlayerGetsGlassPieceSignal obj)
    {
        _pieceImage.gameObject.SetActive(true);
        var piece = _actualGlassPieceManager.GlassPieces().Single(x => x.GlassPiece.PieceId == obj.PieceOfGlass);
        SetSpriteOfCanvas(piece.SpriteToShow);
    }

    private void SetSpriteOfCanvas(Sprite pieceSpriteToShow)
    {
        _pieceImage.sprite = pieceSpriteToShow;
    }


    private void ShowFlashBackWhenMirrorIsCompleted(
        ShowFlashBackWhenMirrorInsideConservatoryIsCompletedLevelTwoSignal obj)
    {
        new ShowOnlyUIMaskSignal().Execute();
        new ShowFlashBackOrImagesSignal().Execute();

        HideGlassPiece();
        HideKeyCard();
        _flashBackViewLevelTwo.gameObject.SetActive(true);
        _flashBackViewLevelTwo.ShowFlashWhenMirrorIsCompleted();
    }

    private void ShowKeyCard()
    {
        _KeyCard.gameObject.SetActive(true);
    }

    private void HideKeyCard()
    {
        _KeyCard.gameObject.SetActive(false);
    }

    protected override void HideImages()
    {
        base.HideImages();
        if (!ServiceLocator.Instance.GetModel<IPlayerModel>().HasKeyCard)
        {
            return;
        }

        _KeyCard.gameObject.SetActive(true);
    }
}