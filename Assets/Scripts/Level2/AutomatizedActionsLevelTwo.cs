﻿using Presentation.Player;
using Signals.EventManager;
using UnityEngine;
using Utils;

public class AutomatizedActionsLevelTwo : MonoBehaviour
{
    [SerializeField] private GameObject _mirrorToRemoveAfterStart;
    [SerializeField] private float _timeToRemoveMirrorAfterStart;
    [SerializeField] private Transform _positionToMovePlayer;
    [SerializeField] private PlayerFacade _playerFacade;
    [SerializeField] private GlassPieceManager _glassPieceManagerInsideConservatory;
    private Timer _timer;
    private IEventManager _eventManager;

    void Start()
    {
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
        _timer = new Timer();
        StartTimerToDeleteMirrorAfterStart();
        _eventManager.AddActionToSignal<InitializeConservatorySectionSignal>(InitializeConservatorySection);
        
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void InitializeConservatorySection(InitializeConservatorySectionSignal obj)
    {
        _playerFacade.transform.position = _positionToMovePlayer.position;
        _glassPieceManagerInsideConservatory.gameObject.SetActive(true);
    }

    private void StartTimerToDeleteMirrorAfterStart()
    {
        _timer.SetTimeToWait(_timeToRemoveMirrorAfterStart);
        _timer.OnTimerEnds += RemoveMirror;
        StartCoroutine(_timer.TimerCoroutine());
    }


    private void RemoveMirror()
    {
        _timer.OnTimerEnds -= RemoveMirror;

        _mirrorToRemoveAfterStart.gameObject.SetActive(false);
    }
}