﻿using System;
using UnityEngine;
using UnityEngine.Assertions;

public class TriggerBoundaries : MonoBehaviour
{
    [SerializeField] private Transform _positionToResetPlayer;
    [SerializeField] private ColliderPassingChecker _colliderPassingChecker;
    [SerializeField] private Transform _playerTransform;

    private void Start()
    {
#if UNITY_EDITOR
        Assert.IsNotNull(_playerTransform);
#endif
        _colliderPassingChecker.OnPlayerPassCollider += ResetPlayerPosition;
    }


    private void OnDestroy()
    {
        _colliderPassingChecker.OnPlayerPassCollider -= ResetPlayerPosition;
    }

    private void ResetPlayerPosition()
    {
        _playerTransform.position = _positionToResetPlayer.position;
    }
}