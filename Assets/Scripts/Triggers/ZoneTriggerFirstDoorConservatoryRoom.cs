﻿using System.Collections;
using System.Collections.Generic;
using Signals.EventManager;
using UnityEngine;

public class ZoneTriggerFirstDoorConservatoryRoom : MonoBehaviour
{
    [SerializeField] private ColliderTriggerZone _colliderTriggerZone;
    private IEventManager _eventManager;
    private bool _hasEnabledBefore;
    private void Start()
    {
        _colliderTriggerZone.OnPlayerTouchCollider += PlayZoneAudio;
    }

    private void OnDestroy()
    {
        _colliderTriggerZone.OnPlayerTouchCollider -= PlayZoneAudio;
    }

    private void PlayZoneAudio(Transform obj)
    {
        if (_hasEnabledBefore)
        {
            return;
        }

        _hasEnabledBefore = true;
        new PlayAudioWhenPlayerEntersFirstRoomConservatorySignal().Execute();
        Destroy(gameObject);
    }
}