﻿using System;

public class DoorColliderPassingChecker : ColliderPassingChecker
{
    private void Start()
    {
        _sizeCollider = _collider.size;
    }
    public override bool HasBeenCrossedCompletelyByPlayer()
    {
        if (_entranceAxis == EntranceAxis.HORIZONTAL)
        {
            return Math.Abs(_positionOfPlayerAtStartEntrance.z - _positionOfPlayerAtExitEntrance.z) >
                   _sizeCollider.z;
        }

        bool hasCrossed = Math.Abs(_positionOfPlayerAtStartEntrance.x - _positionOfPlayerAtExitEntrance.x) >
                          _sizeCollider.z;
        return hasCrossed;
    }
}