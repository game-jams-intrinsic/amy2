﻿using UnityEngine;

public class TriggerChecker : MonoBehaviour
{
    [SerializeField] private Transform _positionToResetPlayer;
    [SerializeField] private Transform _playerTransform;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            ResetPlayerPosition();
        }
    }

    private void ResetPlayerPosition()
    {
        _playerTransform.position = _positionToResetPlayer.position;
    }
}