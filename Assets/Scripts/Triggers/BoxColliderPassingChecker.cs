﻿using UnityEngine;

public class BoxColliderPassingChecker : ColliderPassingChecker
{

    private void Start()
    {
        _sizeCollider = _collider.size;
    }


    public override bool HasBeenCrossedCompletelyByPlayer()
    {
        if (_entranceAxis == EntranceAxis.HORIZONTAL)
        {
            return   Mathf.Abs(Mathf.Round(_positionOfPlayerAtStartEntrance.z - _positionOfPlayerAtExitEntrance.z)) >=
                               _sizeCollider.z;
        }

        Debug.Log($"{_positionOfPlayerAtStartEntrance.x - _positionOfPlayerAtExitEntrance.x} SIZE {_sizeCollider.x}");
        bool hasCrossed =
            Mathf.Abs(_positionOfPlayerAtStartEntrance.x - _positionOfPlayerAtExitEntrance.x) >
            _sizeCollider.x;
        return hasCrossed;
    }
}