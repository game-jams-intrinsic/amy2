﻿using Presentation.Player;
using UnityEngine;

public class PlayerAutomatization : MonoBehaviour
{

    void Start()
    {
        new DisablePlayerMovementSignal().Execute();
        new DisableMouseMovementSignal().Execute();
    }

}
