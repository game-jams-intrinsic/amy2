﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GizmoForward : MonoBehaviour
{
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawRay(transform.position, transform.forward );
    }
}
