﻿using System;
using App.PlayerModelInfo;
using UnityEngine;
using Utils;

public class KeyCardDoorChecker : MonoBehaviour
{
    [SerializeField] private DoorLockedByKeyCard _doorLockedByKeyCard;
    private IPlayerModel _playerModel;

    private void Start()
    {
        _playerModel = ServiceLocator.Instance.GetModel<IPlayerModel>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag("Player"))
        {
            if (_playerModel.HasKeyCard)
            {
                return;
            }

            _doorLockedByKeyCard.ShowTextToAdvisePlayerToGetKeyCard();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform.CompareTag("Player"))
        {
            new ShowTextOnCanvasSignal {TextToShow = String.Empty}.Execute();
        }
    }
}