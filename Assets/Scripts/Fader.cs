﻿using System;
using Signals.EventManager;
using UnityEngine;
using UnityEngine.UI;
using Utils;

public partial class Fader : MonoBehaviour
{
    private IEventManager _eventManager;
    [SerializeField] private Image _image;
    [SerializeField] private float _timerToFade, _timerToWaitOnFade;
    public event Action OnChangePieceGlassManager = delegate { };
    private SwitcherFade _switcherFade;
    private Timer _timer;

    void Awake()
    {
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
        _eventManager.AddActionToSignal<StartFadeSignal>(StartFade);
        _eventManager.AddActionToSignal<StopFadeSignal>(StopFade);
    }

    private void Start()
    {
        _timer = new Timer();
        _switcherFade = new SwitcherFade(_image, _timerToFade, true);
    }

    private void StopFade()
    {
        _timer.OnTimerEnds -= StopFade;

        StopFade(null);
    }

    private void StopFade(StopFadeSignal obj)
    {
        _switcherFade = new SwitcherFade(_image, _timerToFade, false);
        StartCoroutine(_switcherFade.Start());
        _switcherFade.OnCoroutineEnd += HandleStopFade;
    }

    private void HandleStopFade(SwitcherFade obj)
    {
        new PlayWhenPlayerGoTroughOutsideMirrorToConservatorySignal().Execute();
        new EnablePlayerMovementSignal().Execute();
    }

    private void StartFade(StartFadeSignal obj)
    {
        _switcherFade.OnCoroutineEnd += MovePlayer;
        StartCoroutine(_switcherFade.Start());
    }

    private void MovePlayer(SwitcherFade obj)
    {
        _switcherFade.OnCoroutineEnd -= MovePlayer;
        new InitializeConservatorySectionSignal().Execute();
        OnChangePieceGlassManager.Invoke();
        _timer.SetTimeToWait(_timerToWaitOnFade);
        _timer.OnTimerEnds += StopFade;
        StartCoroutine(_timer.TimerCoroutine());
    }
}