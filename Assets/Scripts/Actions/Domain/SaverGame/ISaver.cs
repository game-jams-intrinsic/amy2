﻿namespace Domain.SaverGame
{
    public interface ISaver
    {
        void SaveGame(SaveGameInfo gameData);
        void DeleteSaveGame();
        void SaveNewGameStatus(bool statusToSave);
        void SaveLastLanguage(string languageKey);
    }
}