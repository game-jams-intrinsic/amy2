﻿using System;
using App.GameData;
using App.GameDataModel;
using Domain.LoaderSaveGame;
using UnityEngine;
using UnityEngine.SceneManagement;
using Utils;

namespace App
{
    public class GameDataStatusLoader : MonoBehaviour
    {
        private IGameModel _gameModel;
        private GameDataInfo _gameData;
        private ILoader _loader;

        public event Action<bool> OnShowContinueButton = delegate { };
        public event Action OnShowPromptToDeleteLastGame = delegate { };

        void Start()
        {
            _gameModel = ServiceLocator.Instance.GetModel<IGameModel>();
            _loader = ServiceLocator.Instance.GetService<ILoader>();

            if (HasSavedGame())
            {
                _gameData = _gameModel.LoadGame();
                OnShowContinueButton.Invoke(true);
                return;
            }

            OnShowContinueButton.Invoke(false);
        }

        private bool HasSavedGame()
        {
            return _loader.HasSavedGame();
        }

        public void DeleteLastGameAndStartNewGame()
        {
            _gameData = null;
            _gameModel.DeleteLastSaveGame();
            StartNewGame();
        }

        public void StartNewGame()
        {
            if (_gameData != null)
            {
                OnShowPromptToDeleteLastGame.Invoke();
                return;
            }

            //TODO CREATE SCENEMANAGER
            SceneManager.LoadScene("LevelStable");
        }

        public void ContinueGame()
        {
            SceneManager.LoadScene("LevelStable");
        }
    }
}