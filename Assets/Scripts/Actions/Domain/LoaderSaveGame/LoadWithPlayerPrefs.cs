﻿using Domain.JsonTranslator;
using UnityEngine;
using Utils;

namespace Domain.LoaderSaveGame
{
    public class LoadWithPlayerPrefs : ILoader
    {
        private IJsonator _jsonator;

        public LoadWithPlayerPrefs()
        {
            _jsonator = ServiceLocator.Instance.GetService<IJsonator>();
        }

        public SaveGameInfo LoadGame()
        {
            if (!PlayerPrefs.HasKey("SaveGame"))
            {
                return new SaveGameInfo();
            }

            var dataToFromJson = PlayerPrefs.GetString("SaveGame");
            SaveGameInfo gameDataJson = _jsonator.FromJson<SaveGameInfo>(dataToFromJson);
            return gameDataJson;
        }

        public bool HasSavedGame()
        {
            return PlayerPrefs.GetInt("HasSavedGame") == 1;
        }

        public string LoadLastLanguage()
        {
            string loadedLanguage = PlayerPrefs.GetString("Language");
            return loadedLanguage;
        }
    }
}