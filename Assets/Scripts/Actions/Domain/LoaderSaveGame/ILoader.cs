﻿namespace Domain.LoaderSaveGame
{
    public interface ILoader
    {
        SaveGameInfo LoadGame();
        bool HasSavedGame();
        string LoadLastLanguage();
    }
}