﻿
using App.GameData;

namespace App.GameDataModel
{
    public interface IGameModel
    {
        void PauseGame();
        void ContinueGame();
        void RestartScene();
        void LoadInitScene();
        void QuitGame();
        void SaveGame(int currentIdSavePoint);
        GameDataInfo LoadGame();
        bool HasSavedGame();
        void SetNewGameStatus(bool statusToSave);
        void DeleteLastSaveGame();
    }
}