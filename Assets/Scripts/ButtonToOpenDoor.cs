﻿using System;
using Presentation.Items;
using UnityEngine;
using Utils;

public class ButtonToOpenDoor : InteractableItem
{
    public event Action OnPlayerInteractWithButton = delegate { };

    private void Start()
    {
        SetTextToShow(ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageText()
            .TextToOpenCloseDoorInGameKey);
    }

    public override void InteractWithObject()
    {
        OnPlayerInteractWithButton.Invoke();
    }

    public override void InteractWithObject(GameObject objectWhichInteract)
    {
        InteractWithObject();
    }
}