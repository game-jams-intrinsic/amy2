﻿using System;
using UnityEngine;

public class DoorStoppedAlarm : MonoBehaviour
{
    public event Action OnDoorStopped = delegate { };

    public void DoorStopped()
    {
        OnDoorStopped.Invoke();
    }
}