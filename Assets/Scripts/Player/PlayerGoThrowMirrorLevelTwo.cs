﻿using Presentation.Player;
using UnityEngine;
using Utils;

[CreateAssetMenu(fileName = "PlayerGoThrowMirrorLevelTwo", menuName = "ActionsToDo/PlayerGoThrowMirrorLevelTwo")]
public class PlayerGoThrowMirrorLevelTwo : ActionToDo
{
    public override void ExecuteAction()
    {
        ServiceLocator.Instance.GetModel<IEndLevelModel>().HasPressedKeyToGoThrowMirror = true;
        new ShowAllLayersInCullingMaskSignal().Execute();
        new SwitchOffAllLightsLevelOneSignal().Execute();    
    }
}