﻿using App.PlayerModelInfo;
using Presentation.Items;
using Signals.EventManager;
using UnityEngine;
using Utils;

namespace Presentation.Player
{
    public class ItemInteractor : MonoBehaviour
    {
        [SerializeField] private float zAxis, _upValueToStartRaycast = 0.7f;
        [SerializeField] private LayerMask _layersToAllowInteractions;

        private PickableItem _pickableItem, _pickedItem;
        private InteractableBase _interactableItem;
        private float _xRotation, _yRotation, _xMouseValue, _yMouseValue;

        private Camera _camera;
        private IPlayerModel _playerModel;
        private bool _stopInteracting = false;

        private IEventManager _eventManager;

        private void Awake()
        {
            _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
        }

        private void Start()
        {
            _eventManager.AddActionToSignal<ShowPictureOnCanvasSignal>(HideTexts);
            _eventManager.AddActionToSignal<HidePictureOnCanvasSignal>(ShowTexts);
        }

        private void ShowTexts(HidePictureOnCanvasSignal obj)
        {
            _stopInteracting = false;
        }

        private void HideTexts(ShowPictureOnCanvasSignal obj)
        {
            _stopInteracting = true;
        }

        public void Init(Camera camera, IPlayerModel playerModel)
        {
            _camera = camera;
            _playerModel = playerModel;
        }

        private void Update()
        {
            RaycastHit raycastHit;
            Physics.Raycast(_camera.transform.position + Vector3.up * _upValueToStartRaycast, _camera.transform.forward,
                out raycastHit, zAxis, _layersToAllowInteractions);
            if (raycastHit.collider == null)
            {
                ResetItemToPick();
                return;
            }

            var itemInFrontOfPlayer = raycastHit.collider.GetComponent<InteractableBase>();
            if (!itemInFrontOfPlayer || !itemInFrontOfPlayer.enabled || _stopInteracting) return;
            _interactableItem = itemInFrontOfPlayer;
            if (_playerModel.CurrentGlassPiece < 0 && itemInFrontOfPlayer.GetComponent<Mirror>())
            {
                _interactableItem = null;
                return;
            }

            _interactableItem.ShowMessageOverItem();
        }


        public void InteractWithItem()
        {
            if (!_interactableItem)
            {
                return;
            }

            if (_interactableItem.gameObject.layer == LayerMask.NameToLayer("GlassPieces") &&
                !PlayerHasAnotherPiece())
            {
                _interactableItem.InteractWithObject();
            }
            else if (_interactableItem.gameObject.layer == LayerMask.NameToLayer("Pickable"))
            {
                _interactableItem.InteractWithObject();
                ResetItemToPick();
            }
            else if (_interactableItem.gameObject.layer == LayerMask.NameToLayer("Interactable"))
            {
                _interactableItem.InteractWithObject(gameObject);
            }
            else if (_interactableItem.gameObject.layer == LayerMask.NameToLayer("DoorInteractable"))
            {
                DoorLockedByKeyCard doorLockedByKeyCard = _interactableItem.GetComponentInParent<DoorLockedByKeyCard>();
                if (doorLockedByKeyCard)
                {
                    if (!_playerModel.HasKeyCard)
                    {
                        doorLockedByKeyCard.SetButtonColourToRed();
                        return;
                    }

                    doorLockedByKeyCard.SetButtonColourToOriginal();
                }

                _interactableItem.InteractWithObject(gameObject);
            }
        }

        private bool PlayerHasAnotherPiece()
        {
            return _playerModel.CurrentGlassPiece != -1;
        }

        private void OnDrawGizmos()
        {
            if (!_camera)
            {
                return;
            }

            // Debug.DrawRay(transform.position, new Vector3(_xMouseValue, _yMouseValue, zAxis), Color.green);
            Debug.DrawRay(_camera.transform.position + Vector3.up * _upValueToStartRaycast,
                _camera.transform.forward * zAxis,
                Color.red);
        }

        public void ResetItemToPick()
        {
            if (!_interactableItem) return;
            _interactableItem.HideMessageOverItem();
            _interactableItem = null;
        }
    }
}