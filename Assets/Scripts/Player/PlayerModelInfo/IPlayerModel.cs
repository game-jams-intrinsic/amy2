﻿namespace App.PlayerModelInfo
{
    public interface IPlayerModel
    {
        void ResetData();
        int CurrentGlassPiece { get; set; }
        bool HasKeyCard { get; set; }
        bool GoThroughInitMenuToCredits { get; set; }
    }
}