﻿namespace App.PlayerModelInfo
{
    public class PlayerModel : IPlayerModel
    {
        public int CurrentGlassPiece { get; set; }
        public bool HasKeyCard { get; set; }
        public bool GoThroughInitMenuToCredits { get; set; }

        public PlayerModel()
        {
            CurrentGlassPiece = -1;
        }
        
        public void ResetData()
        {
        }
    }
}