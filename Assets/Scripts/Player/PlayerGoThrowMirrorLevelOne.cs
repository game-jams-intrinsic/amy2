﻿using Presentation.Player;
using UnityEngine;
using Utils;

[CreateAssetMenu(fileName = "PlayerGoThrowMirrorLevelOne", menuName = "ActionsToDo/PlayerGoThrowMirrorLevelOne")]
public class PlayerGoThrowMirrorLevelOne : ActionToDo
{
    public override void ExecuteAction()
    {
        ServiceLocator.Instance.GetModel<IEndLevelModel>().HasPressedKeyToGoThrowMirror = true;
        new ShowAllLayersInCullingMaskSignal().Execute();
        new SwitchOffAllLightsLevelOneSignal().Execute();
    }
}