﻿namespace Presentation.Player
{
    public interface IEndLevelModel
    {
        bool HasPressedKeyToGoThrowMirror { get; set; }
        bool HasEndedAudioMirrorIsCompleted { get; set; }
    }
}