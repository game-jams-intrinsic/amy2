﻿using System;

namespace Domain
{
    [Serializable]
    public class SaveGameInfo
    {
        public int LastLevelCompletedSaved;

        public SaveGameInfo()
        {
            LastLevelCompletedSaved = -1;
        }
    }
}