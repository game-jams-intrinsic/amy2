﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Domain
{
    [Serializable]
    public class SaveGameData
    {
        private SaveGameInfo _saveGameInfo;

        public SaveGameData()
        {
            _saveGameInfo = new SaveGameInfo();
        }

        public void AddInfo(int lastLevelCompleted)
        {
            bool HasToUpgradeLastCompletedLevel(SaveGameInfo saveGameCharacterInfo)
            {
                return saveGameCharacterInfo.LastLevelCompletedSaved < lastLevelCompleted;
            }

            if (lastLevelCompleted < 0)
            {
                throw new Exception("Last level completed to save is < 0");
            }


            if (ExistsSaveGame(_saveGameInfo))
            {
                if (!HasToUpgradeLastCompletedLevel(_saveGameInfo))
                {
                    return;
                }
            }

            var infoToSave = new SaveGameInfo()
            {
                LastLevelCompletedSaved = lastLevelCompleted
            };
            _saveGameInfo = infoToSave;
        }

        private bool ExistsSaveGame(SaveGameInfo lastSaveGame)
        {
            return lastSaveGame != null && lastSaveGame.LastLevelCompletedSaved != -1;
        }
    }
}