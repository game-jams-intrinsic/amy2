﻿using Signals.EventManager;
using UnityEngine;
using Utils;

public class LightManagerLevelForth : MonoBehaviour
{
    [SerializeField] private float _timeToSwitchOffFirstLight = 5, _timeToSwitchOnSecondLight = 5;

    [Header("First Part")] [SerializeField]
    private Light _firstLight;

    [SerializeField] private Light _secondLight;
    [SerializeField] private Light _thirdLight;

    [Header("Second Part")] [SerializeField]
    private Light _secondPartLightCenter1;

    [SerializeField] private Light _secondPartLightCenter2;


    [Header("Third Part")] [SerializeField]
    private Light _thirdLightRightDoor;

    [SerializeField] private Light _thirdLightLefttDoor;
    [SerializeField] private Light _thirdLightCenterDoor;
    [SerializeField] private Light _thirdLightCenterLightOne;
    [SerializeField] private Light _thirdLightCenterLightTwo;


    private IEventManager _eventManager;

    private SwitcherLight _switcherOffFirstLightFirstPart,
        _switcherOffSecondLightFirstPart,
        _switcherOffThirdLightFirstPart;

    private SwitcherLight _switcherOffFirstLightSecondPart, _switcherOffSecondLightSecondPart;

    private void Awake()
    {
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
    }

    void Start()
    {
        _firstLight.enabled = false;
        _secondLight.enabled = false;
        _thirdLight.enabled = false;

        _secondPartLightCenter1.enabled = false;
        _secondPartLightCenter2.enabled = false;

        _thirdLightRightDoor.enabled = false;
        _thirdLightLefttDoor.enabled = false;
        _thirdLightCenterDoor.enabled = false;
        _thirdLightCenterLightOne.enabled = false;
        _thirdLightCenterLightTwo.enabled = false;

        _eventManager.AddActionToSignal<EnableFirstLightSignal>(EnableFirstLight);
        _eventManager.AddActionToSignal<EnableLightCenterThirdPartSignal>(EnableThirdLightsCenter);
        _eventManager.AddActionToSignal<EnableLightRightDoorThirdPartSignal>(EnableRightThirdDoorLight);
        _eventManager.AddActionToSignal<EnableLightLeftDoorThirdPartSignal>(EnableLeftThirdDoorLight);
        _eventManager.AddActionToSignal<EnableLightCenterDoorThirdPartSignal>(EnableLightCenterDoorThirdPart);
        _eventManager.AddActionToSignal<DisableSecondLightSignal>(DisableSecondLight);
        _eventManager.AddActionToSignal<DisableReturnToSecondPartSignal>(DisableAllLightsSecondPart);

        _eventManager.AddActionToSignal<StartSwitcherFirstLightlevelZeroSignal>(StartSwitcherLightFirst);
        _eventManager.AddActionToSignal<StartSwitcherSecondLightSignal>(StartSwitcherSecondLight);

        _switcherOffFirstLightFirstPart = new SwitcherLight(_firstLight, _timeToSwitchOffFirstLight, true, 0);
        _switcherOffSecondLightFirstPart = new SwitcherLight(_secondLight, _timeToSwitchOffFirstLight, true, 0);
        _switcherOffThirdLightFirstPart = new SwitcherLight(_thirdLight, _timeToSwitchOffFirstLight, true, 0);

        _switcherOffFirstLightSecondPart =
            new SwitcherLight(_secondLight, _timeToSwitchOnSecondLight, false, _secondLight.intensity);
        _switcherOffSecondLightSecondPart =
            new SwitcherLight(_secondLight, _timeToSwitchOnSecondLight, false, _secondLight.intensity);
    }

    private void DisableAllLightsSecondPart(DisableReturnToSecondPartSignal obj)
    {
        _firstLight.enabled = false;
        _secondLight.enabled = false;
        _thirdLight.enabled = false;

        _secondPartLightCenter1.enabled = false;
        _secondPartLightCenter2.enabled = false;
    }


    private void OnDestroy()
    {
        _eventManager?.RemoveActionFromSignal<EnableFirstLightSignal>(EnableFirstLight);
        _eventManager?.RemoveActionFromSignal<EnableLightCenterThirdPartSignal>(EnableThirdLightsCenter);
        _eventManager?.RemoveActionFromSignal<EnableLightRightDoorThirdPartSignal>(EnableRightThirdDoorLight);
        _eventManager?.RemoveActionFromSignal<EnableLightLeftDoorThirdPartSignal>(EnableLeftThirdDoorLight);
        _eventManager?.RemoveActionFromSignal<EnableLightLeftDoorThirdPartSignal>(EnableLeftThirdDoorLight);
        _eventManager?.RemoveActionFromSignal<DisableSecondLightSignal>(DisableSecondLight);

        _eventManager?.RemoveActionFromSignal<StartSwitcherFirstLightlevelZeroSignal>(StartSwitcherLightFirst);
        _eventManager?.RemoveActionFromSignal<StartSwitcherSecondLightSignal>(StartSwitcherSecondLight);
    }

    private void EnableLightCenterDoorThirdPart(EnableLightCenterDoorThirdPartSignal obj)
    {
        _thirdLightCenterDoor.enabled = true;
    }

    private void StartSwitcherLightFirst(StartSwitcherFirstLightlevelZeroSignal obj)
    {
        _firstLight.enabled = false;
        _secondLight.enabled = false;
        _thirdLight.enabled = false;
        // StartCoroutine(_switcherFirstLight.Start());
    }

    private void StartSwitcherSecondLight(StartSwitcherSecondLightSignal obj)
    {
        _secondPartLightCenter1.enabled = true;
        _secondPartLightCenter2.enabled = true;

        StartCoroutine(_switcherOffFirstLightSecondPart.Start());
        StartCoroutine(_switcherOffSecondLightSecondPart.Start());
    }

    private void DisableSecondLight(DisableSecondLightSignal obj)
    {
        _secondPartLightCenter1.enabled = false;
        _secondPartLightCenter2.enabled = false;
    }

    private void EnableLeftThirdDoorLight(EnableLightLeftDoorThirdPartSignal obj)
    {
        _thirdLightLefttDoor.enabled = true;
    }

    private void EnableRightThirdDoorLight(EnableLightRightDoorThirdPartSignal obj)
    {
        _thirdLightRightDoor.enabled = true;
    }

    private void EnableThirdLightsCenter(EnableLightCenterThirdPartSignal obj)
    {
        Debug.Log("Ponemos luces centro parte 3");
        _thirdLightCenterLightOne.enabled = true;
        _thirdLightCenterLightTwo.enabled = true;
    }

    private void EnableFirstLight(EnableFirstLightSignal signal)
    {
        _firstLight.enabled = true;
    }
}