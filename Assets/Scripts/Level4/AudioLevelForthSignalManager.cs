﻿using Signals.EventManager;
using UnityEngine;
using Utils;

public class AudioLevelForthSignalManager : MonoBehaviour
{
    [SerializeField] private AudioToPlay _audiosToPlay;

    [SerializeField] private AudioSource _audioSourceBackgroundSoundLevelZero;

    private IEventManager _eventManager;
    private Timer _timer;

    void Awake()
    {
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
    }

    private void Start()
    {
        _timer = new Timer();
        _eventManager.AddActionToSignal<MuteAllAudiosBeforeExitLevelZeroSignal>(MuteAllAudios);
        _eventManager.AddActionToSignal<PlayWhenPlayerEnterThirdPartSoundSignal>(WhenPlayerEnterThirdPartSound);
        _audioSourceBackgroundSoundLevelZero.Play();
    }

    private void WhenPlayerEnterThirdPartSound(PlayWhenPlayerEnterThirdPartSoundSignal obj)
    {
        var audioToPlay = _audiosToPlay.GetAudio("WhenPlayerEnterThirdPartSound",
            ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageKey());
        audioToPlay.Play();
        Timer timerToEnablePauseMenuAgain = new Timer();
        timerToEnablePauseMenuAgain.OnTimerEnds += EnablePauseMenuAgain;
        timerToEnablePauseMenuAgain.SetTimeToWait(audioToPlay.clip.length);
        StartCoroutine(timerToEnablePauseMenuAgain.TimerCoroutine());
        
        Timer timerToStartCameraMovement = new Timer();
        timerToStartCameraMovement.OnTimerEnds += StartCameraMovement;
        timerToStartCameraMovement.SetTimeToWait(audioToPlay.clip.length/2);
        StartCoroutine(timerToStartCameraMovement.TimerCoroutine());
        
    }

    private void StartCameraMovement()
    {
        new StartMovementCameraAfterEnterCenterZoneSignal().Execute();
    }

    private void EnablePauseMenuAgain()
    {
        _timer.OnTimerEnds -= EnablePauseMenuAgain;

        new EnablePauseMenuSignal().Execute();
    }

    // private void PlayPlayerEnterThirdRoomSecondPartSound()
    // {
    //     _timer.OnTimerEnds -= PlayPlayerEnterThirdRoomSecondPartSound;
    //     _audiosToPlay.GetAudio("WhenPlayerEnterThirdPartSoundSecond",
    //         ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageKey()).Play();
    // }


    private void MuteAllAudios(MuteAllAudiosBeforeExitLevelZeroSignal obj)
    {
        _audioSourceBackgroundSoundLevelZero.Stop();
    }
}