﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;

namespace Utils
{
    public class FaderText : IEnumerator
    {
        public event Action OnCoroutineEnd = delegate { };
        private float _timeToSwitch, _timeToStay;
        private bool _fadeOn;
        private TextMeshProUGUI _text;
        private float _initAlpha, _destinationAlpha;

        public FaderText(TextMeshProUGUI text)
        {
            _text = text;
        }

        public void SetFadeType(bool fadeIn)
        {
            if (fadeIn)
            {
                _initAlpha = 0;
                _destinationAlpha = 1;
            }
            else
            {
                _initAlpha = 1;
                _destinationAlpha = 0;
            }
        }

        public void SetTimeToFade(float timeToShow)
        {
            _timeToSwitch = timeToShow;
        }

        public void SetTimeToStay(float durationToStay)
        {
            _timeToStay = durationToStay;
        }

        public IEnumerator FadeIn()
        {
            float counter = 0f;

            while (counter < _timeToSwitch)
            {
                counter += Time.deltaTime;
                _text.alpha = Mathf.Lerp(_initAlpha, _destinationAlpha, counter / _timeToSwitch);
                yield return null;
            }

            _text.alpha = _destinationAlpha;
            OnCoroutineEnd.Invoke();
        }
        
        public bool MoveNext()
        {
            return false;
            // throw new System.NotImplementedException();
        }

        public void Reset()
        {
            // throw new System.NotImplementedException();
        }

        public object Current { get; }


    }
}