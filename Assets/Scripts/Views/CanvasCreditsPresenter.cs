﻿using App.PlayerModelInfo;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using Utils;
using Utils.Input;

public class CanvasCreditsPresenter : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI _creditsTitle, _programmersTitle, _artTitle, _designTitle, _textToExit, _textContinue, _textVoicesTitle, _textVoicesSpanish, _textVoicesEnglish;

    [SerializeField] private GameObject _credtisPage, _almaImage;
    [SerializeField] private float _timeToShowAlmaImage;

    private ILanguageManager _languageManager;
    private ReadInputPlayer _readInputPlayer;
    private Timer _timer;

    private void Awake()
    {
        _languageManager = ServiceLocator.Instance.GetService<ILanguageManager>();
        _readInputPlayer = ServiceLocator.Instance.GetService<ReadInputPlayer>();
        Cursor.visible = false;
    }

    private void Start()
    {
        _timer = new Timer();
        _almaImage.gameObject.SetActive(true);
        _credtisPage.gameObject.SetActive(false);
        _timer.SetTimeToWait(_timeToShowAlmaImage);
        _timer.OnTimerEnds += ShowCredits;
        StartCoroutine(_timer.TimerCoroutine());
    }

    private void ShowCredits()
    {
        _timer.OnTimerEnds -= ShowCredits;

        _almaImage.gameObject.SetActive(false);
        _credtisPage.gameObject.SetActive(true);
        _readInputPlayer.OnDebugEscPressed += ExitCredits;
        SetTranslationText();
    }

    private void OnDestroy()
    {
        _readInputPlayer.OnDebugEscPressed -= ExitCredits;
        ServiceLocator.Instance.UnregisterService<ILanguageManager>();
        ServiceLocator.Instance.UnregisterService<ReadInputPlayer>();
    }

    private void ExitCredits()
    {
        SceneManager.LoadScene("Init");
    }

    private void SetTranslationText()
    {
        _creditsTitle.SetText(_languageManager.GetActualLanguageText().CreditsTitleCreditsSceneKey);
        _programmersTitle.SetText(_languageManager.GetActualLanguageText().ProgrammersTitleCreditsSceneKey);
        _artTitle.SetText(_languageManager.GetActualLanguageText().Art3DTitleCreditsSceneKey);
        _designTitle.SetText(_languageManager.GetActualLanguageText().DesignTitleCreditsSceneKey);
        _textToExit.SetText(_languageManager.GetActualLanguageText().TextToExitCreditsSceneKey);
        _textContinue.SetText(_languageManager.GetActualLanguageText().TextContinueCreditsSceneKey);
        _textVoicesTitle.SetText(_languageManager.GetActualLanguageText().TextVoicesTitle);
        _textVoicesSpanish.SetText(_languageManager.GetActualLanguageText().TextVoicesSpanish);
        _textVoicesEnglish.SetText(_languageManager.GetActualLanguageText().TextVoicesEnglish);
    }
}