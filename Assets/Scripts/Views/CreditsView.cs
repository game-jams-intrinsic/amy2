﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils;

public class CreditsView : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _creditsTitle,
        _programmersTitle,
        _art3DTitle,
        _art2DTitle,
        _musicTitle,
        _designTitle,
        _textToExit,
        _textVoicesTitle,
        _textVoicesSpanish,
        _textVoicesEnglish;

    public event Action OnBackButtonIsPressed = delegate { };

    // private ILanguageManager _languageManager;
    [SerializeField] private Button _backButton;

    private void Awake()
    {
        // _languageManager = ServiceLocator.Instance.GetService<ILanguageManager>();
    }

    private void Start()
    {
        _backButton.onClick.AddListener(BackButton);
    }

    private void BackButton()
    {
        OnBackButtonIsPressed.Invoke();
    }

    public void SetLanguageText(ILanguageManager languageManager)
    {
        _creditsTitle.SetText(languageManager.GetActualLanguageText().CreditsTitleCreditsSceneKey);
        _creditsTitle.SetText(languageManager.GetActualLanguageText().CreditsTitleCreditsSceneKey);
        _programmersTitle.SetText(languageManager.GetActualLanguageText().ProgrammersTitleCreditsSceneKey);
        _art3DTitle.SetText(languageManager.GetActualLanguageText().Art3DTitleCreditsSceneKey);
        _art2DTitle.SetText(languageManager.GetActualLanguageText().Art2DTitleCreditsSceneKey);
        _musicTitle.SetText(languageManager.GetActualLanguageText().MusiciansTitleCreditsSceneKey);
        _designTitle.SetText(languageManager.GetActualLanguageText().DesignTitleCreditsSceneKey);
        _textToExit.SetText(languageManager.GetActualLanguageText().BackButtonTextInitSceneKey);
        _textVoicesTitle.SetText(languageManager.GetActualLanguageText().TextVoicesTitle);
        _textVoicesSpanish.SetText(languageManager.GetActualLanguageText().TextVoicesSpanish);
        _textVoicesEnglish.SetText(languageManager.GetActualLanguageText().TextVoicesEnglish);
    }
}