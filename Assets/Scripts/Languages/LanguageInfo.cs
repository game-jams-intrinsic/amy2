﻿using System;

[Serializable]
public struct LanguageInfo
{
    public LanguageText LanguageText;
    public LanguagesKeys LanguagesKey;
}