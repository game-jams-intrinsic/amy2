﻿using UnityEngine;

[CreateAssetMenu(fileName = "SpanishVersion", menuName = "Languages/Spanish")]
public class SpanishLanguageText : LanguageText
{
}