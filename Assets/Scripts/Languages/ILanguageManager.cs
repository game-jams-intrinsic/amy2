﻿public interface ILanguageManager
{
    LanguageText GetActualLanguageText();
    LanguagesKeys GetActualLanguageKey();
    void SetActualLanguageText(LanguagesKeys languagesKeyToChange);
    void SaveCurrentLanguage();
}