﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.LoaderSaveGame;
using Domain.SaverGame;
using UnityEngine;
using Utils;

public class LanguageManager : MonoBehaviour, ILanguageManager
{
    [SerializeField] private List<LanguageInfo> _languageInfos;
    private LanguageText _actualLanguageText;
    private LanguagesKeys _actualLanguageKey;
    private ISaver _saver;
    private ILoader _loader;

    private void Awake()
    {
        _loader = ServiceLocator.Instance.GetService<ILoader>();
        _saver = ServiceLocator.Instance.GetService<ISaver>();
    }

    public void Start()
    {
        LoadCurrentLanguage();
    }

    public LanguageText GetActualLanguageText()
    {
        return _actualLanguageText;
    }

    public LanguagesKeys GetActualLanguageKey()
    {
        return _actualLanguageKey;
    }

    public void SetActualLanguageText(LanguagesKeys languagesKeyToChange)
    {
        _actualLanguageText = _languageInfos.Single(x => x.LanguagesKey == languagesKeyToChange).LanguageText;
        _actualLanguageKey = languagesKeyToChange;
    }

    public void SaveCurrentLanguage()
    {
        _saver.SaveLastLanguage(_actualLanguageKey.ToString());
    }

    private void LoadCurrentLanguage()
    {
        var loadLastLanguage = _loader.LoadLastLanguage();
        if (String.IsNullOrEmpty(loadLastLanguage))
        {
            SetActualLanguageText(LanguagesKeys.SPA);
        }
        else
        {
            LanguagesKeys key = (LanguagesKeys) Enum.Parse(typeof(LanguagesKeys), loadLastLanguage);
            SetActualLanguageText(key);
        }
    }
}