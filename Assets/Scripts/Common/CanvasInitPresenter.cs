﻿using System;
using Domain;
using Domain.LoaderSaveGame;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Utils;

public class CanvasInitPresenter : MonoBehaviour
{
    [SerializeField] private Button _buttonStart, _buttonExit, _buttonOptions, _buttonCredits;

    [SerializeField]
    private TextMeshProUGUI _buttonStartText, _buttonExitText, _buttonOptionsText, _buttonCreditsText, _textVersion;

    [SerializeField] private OptionsView _optionsView;
    [SerializeField] private CreditsView _creditsView;
    [SerializeField] private Transform _initView;
    private SceneChanger _sceneChanger;

    private ILanguageManager _languageManager;
    private ILoader _loader;

    private void Awake()
    {
        _languageManager = ServiceLocator.Instance.GetService<ILanguageManager>();
        _loader = ServiceLocator.Instance.GetService<ILoader>();
        _sceneChanger = ServiceLocator.Instance.GetService<SceneChanger>();
        Time.timeScale = 1;
    }

    void Start()
    {
        _textVersion.SetText($"v.{Application.version}");

        HideOptions();
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        _buttonStart.onClick.AddListener(StartGame);
        _buttonExit.onClick.AddListener(ExitGame);
        _buttonOptions.onClick.AddListener(ShowOptions);
        _buttonCredits.onClick.AddListener(GoToCredits);
        _optionsView.OnBackButtonIsPressed += HideOptions;
        _optionsView.OnEnglishLanguageButtonIsPressed += ChangeToEnglishLanguage;
        _optionsView.OnSpanishLanguageButtonIsPressed += ChangeToSpanishLanguage;
        _creditsView.OnBackButtonIsPressed += HideCredits;
        SetLanguageTexts();
        SetStartButtonLanguage();
        _creditsView.gameObject.SetActive(false);
        _optionsView.gameObject.SetActive(false);
        _initView.gameObject.SetActive(true);
    }
    

    private void SetStartButtonLanguage()
    {
        if (_loader.HasSavedGame())
        {
            _buttonStartText.SetText(_languageManager.GetActualLanguageText().ContinueGameButtonInitSceneKey);
            return;
        }

        _buttonStartText.SetText(_languageManager.GetActualLanguageText().PlayButtonInitSceneKey);
    }


    private void GoToCredits()
    {
        _creditsView.gameObject.SetActive(true);
        _creditsView.SetLanguageText(_languageManager);
        _initView.gameObject.SetActive(false);
    }

    private void ChangeToSpanishLanguage()
    {
        _languageManager.SetActualLanguageText(LanguagesKeys.SPA);
        _languageManager.SaveCurrentLanguage();

        SetLanguageTexts();
    }

    private void ChangeToEnglishLanguage()
    {
        _languageManager.SetActualLanguageText(LanguagesKeys.ENG);
        _languageManager.SaveCurrentLanguage();
        SetLanguageTexts();
    }

    private void HideOptions()
    {
        _initView.gameObject.SetActive(true);
        _optionsView.gameObject.SetActive(false);
        SetStartButtonLanguage();
    }

    private void HideCredits()
    {
        _initView.gameObject.SetActive(true);
        _creditsView.gameObject.SetActive(false);
        SetStartButtonLanguage();
    }

    private void ShowOptions()
    {
        _initView.gameObject.SetActive(false);
        _optionsView.gameObject.SetActive(true);
        _optionsView.SetLanguageText(_languageManager);
    }

    private void SetLanguageTexts()
    {
        _buttonCreditsText.SetText(_languageManager.GetActualLanguageText().CreditsButtonInitSceneKey);
        _buttonExitText.SetText(_languageManager.GetActualLanguageText().ExitButtonInitSceneKey);
        _buttonOptionsText.SetText(_languageManager.GetActualLanguageText().OptionsButtonInitSceneKey);
        _optionsView.SetLanguageText(_languageManager);
    }

    private void OnDestroy()
    {
        _buttonStart.onClick.RemoveListener(StartGame);
        _buttonExit.onClick.RemoveListener(ExitGame);
    }

    private void ExitGame()
    {
        Application.Quit();
    }

    private void StartGame()
    {
        if (_loader.HasSavedGame())
        {
            _sceneChanger.LoadCurrentLevelFromInitScene(_loader.LoadGame().LastLevelCompletedSaved);

            return;
        }

        _sceneChanger.GoToFirstLevel();
    }
}