﻿using System;
using UnityEngine;

public class ColliderTriggerZone : MonoBehaviour
{
    public event Action<Transform> OnPlayerTouchCollider = delegate { };

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag("Player"))
        {
            OnPlayerTouchCollider.Invoke(other.transform);
        }
    }
}
