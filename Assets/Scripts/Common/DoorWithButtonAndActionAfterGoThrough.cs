﻿using UnityEngine;
using UnityEngine.Serialization;

public class DoorWithButtonAndActionAfterGoThrough : Door
{
    [SerializeField] private ActionToDo _actionToDo;

    [FormerlySerializedAs("_doorButton")] [SerializeField]
    private ButtonToOpenDoor buttonToOpenDoor;

    private void Start()
    {
        _colliderPassingChecker.OnPlayerPassCollider += ExecuteAction;
        buttonToOpenDoor.OnPlayerInteractWithButton += InteractWith;
    }

    private void ExecuteAction()
    {
        if (!_actionToDo)
        {
            return;
        }

        _actionToDo.ExecuteAction();
    }
}