﻿using System.Collections.Generic;
using System.Linq;
using Presentation.Items;
using Presentation.Player;
using UnityEngine;
using Utils;

public class Mirror : InteractableItem
{
    [SerializeField] private List<MirrorPieces> _mirrorPieceses;

    private void Start()
    {
        Reset();
        SetTextToShow(ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageText().TextToPutPieceInMirrorInGameKey);
    }

    private void Reset()
    {
        foreach (var pieces in _mirrorPieceses)
        {
            pieces.Piece.gameObject.SetActive(false);
        }
    }

    private void AddMirrorPiece(int idPiece)
    {
        MirrorPieces mirrorPieces = _mirrorPieceses.Single(x => x.IdPiece == idPiece);
        mirrorPieces.Piece.gameObject.SetActive(true);
        new PlayerPutsGlassPieceOnMirrorSignal() {GlassPieceId = idPiece}.Execute();
    }

    public override void InteractWithObject()
    {
    }

    public override void InteractWithObject(GameObject objectWhichInteract)
    {
        Debug.Log(objectWhichInteract.GetComponent<PlayerFacade>().PlayerModel.CurrentGlassPiece);
        AddMirrorPiece(objectWhichInteract.GetComponent<PlayerFacade>().PlayerModel.CurrentGlassPiece);
        new RemoveGlassPieceOfPlayerSignal().Execute();
    }
}