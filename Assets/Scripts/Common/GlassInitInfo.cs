﻿using System;
using UnityEngine;

[Serializable]
public class GlassInitInfo
{
    public GlassPiece GlassPiece;
    public bool IsEnabledAtFirst;
    public Sprite SpriteToShow;
    public String Localization;
}