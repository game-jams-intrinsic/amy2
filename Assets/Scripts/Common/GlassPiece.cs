﻿using System;
using Presentation.Items;
using UnityEngine;
using Utils;

public class GlassPiece : PickableItem
{
    [SerializeField] private int _pieceId;

    public int PieceId => _pieceId;

    private void Start()
    {
        SetTextToShow(ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageText().TextToGetGlassPieceInGameKey);
    }

    public override void InteractWithObject(GameObject objectWhichInteract)
    {
        InteractWithObject();
    }
    
    public override void InteractWithObject()
    {
        var signal = new PlayerGetsGlassPieceSignal {PieceOfGlass = _pieceId};
        signal.Execute();
        HideMessageOverItem();
        Destroy(gameObject);
    }

}