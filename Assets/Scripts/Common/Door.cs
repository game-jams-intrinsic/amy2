﻿using UnityEngine;

public abstract class Door : MonoBehaviour
{
    [SerializeField] protected Animator _animator;
    [SerializeField] protected ColliderPassingChecker _colliderPassingChecker;
    private static readonly int Close = Animator.StringToHash("Close");
    private static readonly int Open = Animator.StringToHash("Open");
    [SerializeField] protected bool _isOpen;
    private bool _isMoving = false;


    protected virtual void OpenDoor()
    {
        _isMoving = true;
        _animator.SetTrigger(Open);
    }


    protected void InteractWith()
    {
        if (_isMoving)
        {
            Debug.Log("FF");
            return;
        }

        if (_isOpen)
        {
            CloseDoor();
        }
        else
        {
            OpenDoor();
        }

        _isOpen = !_isOpen;
        Debug.Log("Interactuamos con Puerta");
    }


    protected virtual void CloseDoor()
    {
        _isMoving = true;

        _animator.SetTrigger(Close);
    }

    protected virtual void DoorStopped()
    {
        _isMoving = false;
    }

    protected void OnDestroy()
    {
        _colliderPassingChecker.OnPlayerPassCollider -= InteractWith;
    }
}