﻿using UnityEngine;

public class TriggerWhenPlayerGoToDoor : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (!other.gameObject.CompareTag("Player"))
        {
            return;
        }

        new PlayWhenTriggerLockedDoorSoundSignal().Execute();
    }
}