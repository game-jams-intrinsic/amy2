﻿using UnityEngine;
using UnityEngine.Serialization;

public class DoorWithButton : Door
{
    [FormerlySerializedAs("_doorButton")] [SerializeField] protected ButtonToOpenDoor buttonToOpenDoor;
    [SerializeField] protected Renderer _buttonRenderer1;
    [SerializeField] protected Material _redColour, _originalColour;
    [SerializeField] protected DoorStoppedAlarm _doorStoppedAlarm;

    protected override void OpenDoor()
    {
        _buttonRenderer1.material = _redColour;
        base.OpenDoor();
    }


    protected override void CloseDoor()
    {
        _buttonRenderer1.material = _redColour;
        base.CloseDoor();
    }

    protected override void DoorStopped()
    {
        _buttonRenderer1.material = _originalColour;
        base.DoorStopped();
    }
    private void Start()
    {
        _colliderPassingChecker.OnPlayerPassCollider += InteractWith;
        buttonToOpenDoor.OnPlayerInteractWithButton += InteractWith;
        _doorStoppedAlarm.OnDoorStopped += DoorStopped;
        if (_isOpen)
        {
            OpenDoor();
        }
    }

    private void OnDestroy()
    {
        base.OnDestroy();
        buttonToOpenDoor.OnPlayerInteractWithButton -= InteractWith;
        _colliderPassingChecker.OnPlayerPassCollider -= InteractWith;
        _doorStoppedAlarm.OnDoorStopped -= DoorStopped;

    }
}