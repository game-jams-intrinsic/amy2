﻿using DG.Tweening;
using UnityEngine;

public abstract class CameraRotatorAutomatizationBase : MonoBehaviour
{
    protected Tweener _tween;
    [SerializeField] protected CameraRotator _cameraRotator;
    [SerializeField] protected Transform _leftDoor, _rightPosition, _centerPosition;

    [SerializeField] protected float
        _timeToMoveToLeftDoor,
        _timeToMoveToRightDoor,
        _timeToMoveToCenterDoor;

    protected abstract void MoveCameraToLeft();

    protected void MoveCameraToCenterAndLaterRight()
    {
        Debug.Log("MOVE CENTER AND RIGHT");
        new EnableLightCenterDoorThirdPartSignal().Execute();

        _tween.onComplete -= MoveCameraToCenterAndLaterRight;
        _tween = _cameraRotator.transform.DOLookAt(_centerPosition.position, _timeToMoveToCenterDoor, AxisConstraint.Y,
            Vector3.up);
        _tween.onComplete += MoveCameraToRight;
    }

    protected void MoveCameraToRight()
    {
        new EnableLightRightDoorThirdPartSignal().Execute();
        Debug.Log("MOVE RIGHT AND CENTER ");
        _tween.onComplete -= MoveCameraToRight;
        _tween = _cameraRotator.transform.DOLookAt(_rightPosition.position, _timeToMoveToRightDoor, AxisConstraint.Y,
            Vector3.up);

        _tween.onComplete += MoveCameraToInitialPositionAndEnd;
    }

    private void MoveCameraToInitialPositionAndEnd()
    {
        Debug.Log("MOVE END");
        _tween.onComplete -= MoveCameraToInitialPositionAndEnd;
        _tween = _cameraRotator.transform.DOLookAt(_centerPosition.position, _timeToMoveToCenterDoor, AxisConstraint.Y,
            Vector3.up);
        _tween.onComplete += EndRotations;
    }

    private void EndRotations()
    {
        Debug.Log("END");
        _tween.onComplete -= EndRotations;
        new EnablePlayerMovementSignal().Execute();
        new IsNotShowingFlashBackOrImagesSignal().Execute();
        new ResumeRotationCameraSignal().Execute();
    }
}