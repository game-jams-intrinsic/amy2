﻿using Signals.EventManager;
using UnityEngine;
using Utils;
using Utils.Input;

public class CameraRotator : MonoBehaviour
{
    private ReadInputPlayer _readInputPlayer;

    [SerializeField] private PlayerMovement _playerMovement;
    [SerializeField] private float _mouseSensivity, _minRotationY, _maxRotationY;

    private float _xMouseValue, _yMouseValue;
    private float _xRotation, _yRotation;
    [SerializeField] private bool _isRotating, _canRotate;
    private Vector2 _mouseInput;
    private IEventManager _eventManager;

    void Awake()
    {
        _readInputPlayer = ServiceLocator.Instance.GetService<ReadInputPlayer>();
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
    }

    private void Start()
    {
        // _readInputPlayer.OnPlayerMovesMouse += GetRotationsValuesOfMouse;
        // _readInputPlayer.OnPlayerStopMovesMouse += StopRotationsValuesOfMouse;
        _eventManager.AddActionToSignal<StopRotationCameraSignal>(StopRotationsValuesOfMouseBySignal);
        _eventManager.AddActionToSignal<ResumeRotationCameraSignal>(ResumeRotationCameraBySignal);
        _canRotate = true;
        // _isRotating = false;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void OnDestroy()
    {
        // _readInputPlayer.OnPlayerMovesMouse -= GetRotationsValuesOfMouse;
        // _readInputPlayer.OnPlayerStopMovesMouse -= StopRotationsValuesOfMouse;

        _eventManager.RemoveActionFromSignal<StopRotationCameraSignal>(StopRotationsValuesOfMouseBySignal);
        _eventManager.RemoveActionFromSignal<ResumeRotationCameraSignal>(ResumeRotationCameraBySignal);
    }

    private void StopRotationsValuesOfMouseBySignal(StopRotationCameraSignal obj)
    {
        _canRotate = false;
    }

    private void ResumeRotationCameraBySignal(ResumeRotationCameraSignal obj)
    {
        _canRotate = true;
    }

    void Update()
    {
        if (!_canRotate)
        {
            return;
        }

        GetRotationValuesCamera();
        RotateCamera();
    }

    private void GetRotationValuesCamera()
    {
        float xMouseValue = _readInputPlayer.MouseMovement.y * Time.deltaTime * _mouseSensivity;
        float yMouseValue = _readInputPlayer.MouseMovement.x * Time.deltaTime * _mouseSensivity;


        _xRotation -= xMouseValue;
        _yRotation -= yMouseValue;

        _xRotation = Mathf.Clamp(_xRotation, _minRotationY, _maxRotationY);
    }

    private void RotateCamera()
    {
        transform.localRotation = Quaternion.Euler(_xRotation, -_yRotation, 0.0f);
        _playerMovement.transform.Rotate(_playerMovement.transform.up * _xMouseValue);
    }

    public void RotateCamera(Quaternion quaternion)
    {
        _xRotation -= quaternion.x;
        _yRotation -= quaternion.y;

        // transform.localRotation = Quaternion.Euler(0, -_yRotation, 0.0f);
        // _playerMovement.transform.Rotate(_playerMovement.transform.up * _xMouseValue);
    }
}