﻿using DG.Tweening;
using Signals.EventManager;
using UnityEngine;
using Utils;

public class CameraRotatorAutomatizationLevelZero : CameraRotatorAutomatizationBase
{
    private IEventManager _eventManager;

    void Awake()
    {
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
    }

    void Start()
    {
        _eventManager.AddActionToSignal<StartMovementCameraAfterEnterCenterZoneSignal>(StartMovementToShowDoors);
    }

    private void StartMovementToShowDoors(StartMovementCameraAfterEnterCenterZoneSignal obj)
    {
        // new StopRotationCameraSignal().Execute();

        MoveCameraToLeft();
    }

    protected override void MoveCameraToLeft()
    {
        new EnableLightLeftDoorThirdPartSignal().Execute();
        _tween = _cameraRotator.transform.DOLookAt(_leftDoor.position, 2, AxisConstraint.Y, Vector3.up);
        _tween.onComplete += MoveCameraToRight;
    }
}