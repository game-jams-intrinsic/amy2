﻿using System;
using Domain.LoaderSaveGame;
using Signals.EventManager;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using Utils;
using Utils.Input;

public class CanvasInGamePresenter : MonoBehaviour
{
    [SerializeField] private PauseMenuView _pauseMenuView;
    [SerializeField] private TextMeshProUGUI _actionToUser;
    private ILanguageManager _languageManager;

    private ReadInputPlayer _readInputPlayer;
    private IEventManager _eventManager;
    private bool _isShowingFlashback, _canMoveAfterMenuPause, _canShowPauseMenu, _gameIsPaused;

    private void Awake()
    {
        _readInputPlayer = ServiceLocator.Instance.GetService<ReadInputPlayer>();
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
        _languageManager = ServiceLocator.Instance.GetService<ILanguageManager>();
        _eventManager.AddActionToSignal<ShowFlashBackOrImagesSignal>(HandleImageOrFlashbackOnUIWhenAreOn);
        _eventManager.AddActionToSignal<DisablePauseMenuSignal>(DisablePauseMenu);
        _eventManager.AddActionToSignal<EnablePauseMenuSignal>(EnablePauseMenu);
        _eventManager.AddActionToSignal<IsNotShowingFlashBackOrImagesSignal>(HandleImageOrFlashbackOnUIWhenAreOff);
        _eventManager.AddActionToSignal<DisableMovementAfterEventStartedSignal>(
            DisableMovementPlayerUntilConversationEnds);
        _eventManager.AddActionToSignal<EnableMovementAfterEventEndedSignal>(
            EnableMovementPlayerAfterConversationEnds);
        

        _canMoveAfterMenuPause = true;
        _isShowingFlashback = false;
        _canShowPauseMenu = true;
    }

    
    void Start()
    {
        _readInputPlayer.OnDebugEscPressed += HandleEscape;
        _pauseMenuView.OnContinueGame += ContinueGame;
        _pauseMenuView.OnExitGame += ExitGame;
        _pauseMenuView.gameObject.SetActive(false);
        SetLanguageTexts();
    }

    private void EnablePauseMenu(EnablePauseMenuSignal obj)
    {
        _canShowPauseMenu = true;
    }

    private void DisablePauseMenu(DisablePauseMenuSignal obj)
    {
        _canShowPauseMenu = false;
        
    }


    private void EnableMovementPlayerAfterConversationEnds(EnableMovementAfterEventEndedSignal obj)
    {
        _canMoveAfterMenuPause = true;
    }

    private void DisableMovementPlayerUntilConversationEnds(DisableMovementAfterEventStartedSignal obj)
    {
        _canMoveAfterMenuPause = false;
    }

    private void SetLanguageTexts()
    {
        _pauseMenuView.SetLanguageTexts(_languageManager);
    }

    private void HandleImageOrFlashbackOnUIWhenAreOn(ShowFlashBackOrImagesSignal obj)
    {
        _isShowingFlashback = true;
        _actionToUser.gameObject.SetActive(false);
        _actionToUser.SetText(String.Empty);
        _readInputPlayer.OnDebugEscPressed -= HandleEscape;
    }

    private void HandleImageOrFlashbackOnUIWhenAreOff(IsNotShowingFlashBackOrImagesSignal obj)
    {
        _readInputPlayer.OnDebugEscPressed += HandleEscape;

        _actionToUser.gameObject.SetActive(true);
        _isShowingFlashback = false;
    }

    private void OnDestroy()
    {
        _readInputPlayer.OnDebugEscPressed -= HandleEscape;
        _pauseMenuView.OnContinueGame -= ContinueGame;
        _pauseMenuView.OnExitGame -= ExitGame;
    }

    private void HandleEscape()
    {
        if (_isShowingFlashback || !_canShowPauseMenu)
        {
            return;
        }

        if (_gameIsPaused)
        {
            ContinueGame();
            return;
        }

        PauseGame();
    }

    private void PauseGame()
    {
        _gameIsPaused = true;
        _readInputPlayer.DisableGameplayInput();
        Cursor.visible = true;
        Time.timeScale = 0;

        Cursor.lockState = CursorLockMode.None;
        _pauseMenuView.gameObject.SetActive(true);
        new StopRotationCameraSignal().Execute();
    }

    private void ExitGame()
    {
        SceneManager.LoadScene("Init");
    }

    private void ContinueGame()
    {
        _gameIsPaused = false;

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        if (_canMoveAfterMenuPause)
        {
            _readInputPlayer.EnableGameplayInput();
        }

        _pauseMenuView.gameObject.SetActive(false);
        Time.timeScale = 1;
        new ResumeRotationCameraSignal().Execute();
    }
}