﻿using System;
using System.Collections;
using System.Collections.Generic;
using Signals.EventManager;
using UnityEngine;
using Utils;

public class DelimitDoorAndColliderThirdPart : MonoBehaviour
{
    private IEventManager _eventManager;
    [SerializeField] private List<Collider> _collidersToMakeRigid;
    [SerializeField] private GameObject _doorToDestroy;
    [SerializeField] private Collider _colliderToActivate;

    private void Awake()
    {
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
    }

    // Start is called before the first frame update
    void Start()
    {
        _eventManager.AddActionToSignal<DisableReturnToSecondPartSignal>(DisableReturnToSecondPart);
    }

    private void DisableReturnToSecondPart(DisableReturnToSecondPartSignal obj)
    {
        foreach (var collider in _collidersToMakeRigid)
        {
            collider.isTrigger = false;
        }

        new DisableSecondLightSignal().Execute();
        Destroy(_doorToDestroy.gameObject);
        _colliderToActivate.isTrigger = false;
    }
}