﻿using Presentation.Player;
using Signals.EventManager;
using Utils;
using Utils.Input;

public class SceneManagerLevelTwo : SceneManagerBaseLevel
{
    void Awake()
    {
        _endLevelModel = ServiceLocator.Instance.GetModel<IEndLevelModel>();
        _readInputPlayer = ServiceLocator.Instance.GetService<ReadInputPlayer>();
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();


        _eventManager.AddActionToSignal<AudioEndLevelTwoAfterCompleteMirrorInsideConservatoryHasEndedSignal>(
            AudioEndLevelTwoInsideConservatoryHasEnded);
    }

    private void AudioEndLevelTwoInsideConservatoryHasEnded(
        AudioEndLevelTwoAfterCompleteMirrorInsideConservatoryHasEndedSignal obj)
    {
        AudioEndLevelHasEnded(null);
    }


    private void Start()
    {
        _readInputPlayer.OnPlayerPressGoThrowMirrorKey += PlayerPressGoThrowMirrorKey;
    }


    protected override void CheckIfCanChangeScene()
    {
        if (_endLevelModel.HasEndedAudioMirrorIsCompleted && _endLevelModel.HasPressedKeyToGoThrowMirror)
        {
            if (_actionToDoWhenPlayerPressToGoThrowMirror)
            {
                _actionToDoWhenPlayerPressToGoThrowMirror.ExecuteAction();
            }

            _endLevelModel.HasEndedAudioMirrorIsCompleted = false;
            _endLevelModel.HasPressedKeyToGoThrowMirror = false;
            new PlayerHasCompletedLevelSignal().Execute();
        }
    }
}