﻿using System;
using Presentation.Player;
using Signals.EventManager;
using UnityEngine;
using UnityEngine.SceneManagement;
using Utils;
using Utils.Input;

public class SceneManagerLevelOne : SceneManagerBaseLevel
{
    void Awake()
    {
        _endLevelModel = ServiceLocator.Instance.GetModel<IEndLevelModel>();
        _readInputPlayer = ServiceLocator.Instance.GetService<ReadInputPlayer>();
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();

        _eventManager.AddActionToSignal<AudioEndLevelOneHasEndedSignal>(AudioEndLevelOneHasEnded);
    }

    private void AudioEndLevelOneHasEnded(AudioEndLevelOneHasEndedSignal obj)
    {
        AudioEndLevelHasEnded(null);
    }

    private void Start()
    {
        _readInputPlayer.OnPlayerPressGoThrowMirrorKey += PlayerPressGoThrowMirrorKey;
    }


    protected override void CheckIfCanChangeScene()
    {
        if (_endLevelModel.HasEndedAudioMirrorIsCompleted && _endLevelModel.HasPressedKeyToGoThrowMirror)
        {
            if (_actionToDoWhenPlayerPressToGoThrowMirror)
            {
                _actionToDoWhenPlayerPressToGoThrowMirror.ExecuteAction();
            }

            _endLevelModel.HasEndedAudioMirrorIsCompleted = false;
            _endLevelModel.HasPressedKeyToGoThrowMirror = false;
            new ShowTextOnCanvasSignal() {TextToShow = String.Empty}.Execute();
            new PlayerHasCompletedLevelSignal().Execute();
            Debug.Log("GO TO LEVEL 2");
            // SceneManager.LoadScene("nivel-2");
        }
    }
}