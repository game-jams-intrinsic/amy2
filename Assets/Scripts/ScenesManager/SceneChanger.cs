﻿using System;
using App.PlayerModelInfo;
using DG.Tweening;
using Signals.EventManager;
using UnityEngine;
using UnityEngine.SceneManagement;
using Utils;

public class SceneChanger : MonoBehaviour
{
    [SerializeField] private ScenesListModel _scenesListModel;
    private IEventManager _eventManager;
    private Sequence _sequence;

    void Awake()
    {
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
    }

    private void Start()
    {
        _eventManager.AddActionToSignal<PlayerHasCompletedLevelSignal>(HandleCompletedLevel);
    }

    private void OnDestroy()
    {
        _eventManager.RemoveActionFromSignal<PlayerHasCompletedLevelSignal>(HandleCompletedLevel);
    }

    private void HandleCompletedLevel(PlayerHasCompletedLevelSignal obj)
    {
        SceneInfo currentSceneInfo =
            _scenesListModel.SceneInfoByName(GetCurrentScene());

        new SavePlayerProgressSignal()
        {
            IdSceneToSave = currentSceneInfo.SceneId,
        }.Execute();

        ChangeLevel();
    }


    private void ChangeLevel()
    {
        SceneInfo nextSceneInfo = _scenesListModel.GetNextScene(GetCurrentScene());

        Debug.Log("Has pasado de nivel");
        SceneManager.LoadScene(nextSceneInfo.SceneName);
    }

    public void RestartScene()
    {
        SceneManager.LoadScene(GetCurrentScene());
    }

    public void GoToFirstLevel()
    {
        SceneManager.LoadScene(_scenesListModel.GetSceneById(0).SceneName);
    }

    public void GoToInitScene()
    {
        SceneManager.LoadScene("Init");
    }

    private String GetCurrentScene()
    {
        return SceneManager.GetActiveScene().name;
    }

    public void LoadCurrentLevelFromInitScene(int gameDataLastLevelCompleted)
    {
        if (gameDataLastLevelCompleted + 1 >= _scenesListModel.GetSceneCount())
        {
            SceneManager.LoadScene(_scenesListModel.GetSceneById(_scenesListModel.GetSceneCount()-1).SceneName);
            return;
        }


        SceneManager.LoadScene(_scenesListModel.GetSceneById(gameDataLastLevelCompleted + 1).SceneName);
    }
}