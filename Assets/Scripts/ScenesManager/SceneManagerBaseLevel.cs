﻿using System;
using Presentation.Player;
using Signals.EventManager;
using UnityEngine;
using Utils.Input;

public abstract class SceneManagerBaseLevel : MonoBehaviour
{
    protected ReadInputPlayer _readInputPlayer;
    protected IEndLevelModel _endLevelModel;
    protected IEventManager _eventManager;
    [SerializeField] protected ActionToDo _actionToDoWhenPlayerPressToGoThrowMirror;

    protected void AudioEndLevelHasEnded(AudioEndLevelHasEndedSignal obj)
    {
        _endLevelModel.HasEndedAudioMirrorIsCompleted = true;
        CheckIfCanChangeScene();
    }

    protected void PlayerPressGoThrowMirrorKey()
    {
        new ShowTextOnCanvasSignal() {TextToShow = String.Empty}.Execute();
        _endLevelModel.HasPressedKeyToGoThrowMirror = true;

        CheckIfCanChangeScene();
    }

    protected abstract void CheckIfCanChangeScene();
}