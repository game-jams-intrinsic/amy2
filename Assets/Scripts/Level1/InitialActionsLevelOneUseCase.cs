﻿using UnityEngine;

public class InitialActionsLevelOneUseCase : MonoBehaviour
{
    void Start()
    {
        Cursor.visible = false;
        new DisablePlayerMovementSignal().Execute();
        new PlayBackgroundMusicLevelOneSignal().Execute();
        new PlayHouseBackGroundMusicLevelOneSignal().Execute();
        new SwitchOnAllLightsLevelOneSignal().Execute();
        new DisableMovementAfterEventStartedSignal().Execute();
        new DisablePauseMenuSignal().Execute();
    }
}