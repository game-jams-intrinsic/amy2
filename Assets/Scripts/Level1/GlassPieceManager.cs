﻿using System.Collections.Generic;
using Signals.EventManager;
using UnityEngine;
using Utils;

public abstract class GlassPieceManager : MonoBehaviour
{
    protected IEventManager _eventManager;
    protected int _idGlassEnabledAtFirst;
    protected int _glassPiecesRecovered;

    public abstract List<GlassInitInfo> GlassPieces();

    private void Awake()
    {
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
    }

    protected void Start()
    {
        _glassPiecesRecovered = 0;
        _eventManager.AddActionToSignal<PlayerPutsGlassPieceOnMirrorSignal>(PlayerHasPutGlassPieceOnMirror);
    }

    protected virtual void PlayerHasPutGlassPieceOnMirror(PlayerPutsGlassPieceOnMirrorSignal obj)
    {
        _glassPiecesRecovered++;
        new HideGlassPieceOfCanvasSignal().Execute();
        new PlayGlassPiecesIsPutInMirrorAudioSignal().Execute();
    }
}