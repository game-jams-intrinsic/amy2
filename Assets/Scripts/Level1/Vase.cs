﻿using UnityEngine;

public class Vase : MonoBehaviour
{
    [SerializeField] public float _valueToMove;
    [SerializeField] private ActionToDo _actionToDo;
    [SerializeField] private Animator _animator;
    private bool _hasBeenTriggered;
    private Rigidbody _rigidbody;
    private static readonly int Destroy1 = Animator.StringToHash("Destroy");

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision other)
    {
        if (_hasBeenTriggered)
        {
            return;
        }

        if (other.collider.gameObject.layer == LayerMask.NameToLayer("Floor"))
        {
            _actionToDo.ExecuteAction();
            Debug.Log("Toca suelo");
            // _animator.SetTrigger(Destroy1);
            _hasBeenTriggered = true;
        }
    }

    public void MoveToLeft()
    {
        _rigidbody.isKinematic = false;

       // Tweener tweener =  transform.DOMoveZ(_valueToMove,1);
       // tweener.OnComplete(RemoveKinematic);
       // Vector3 destination = transform.position + Vector3.right * _valueToMove;
       // MoveCoroutine moveCoroutine = new MoveCoroutine(transform, transform.position,
       //     destination, _speedMovement);
       // moveCoroutine.OnCoroutineEnd += RemoveKinematic;
       // StartCoroutine(moveCoroutine.Start());
    }

    private void RemoveKinematic()
    {
        // obj.OnCoroutineEnd -= RemoveKinematic;
        // _rigidbody.isKinematic = false;
    }
}