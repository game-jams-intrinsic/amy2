﻿using Signals.EventManager;
using UnityEngine;
using Utils;

public class AudioManagerLevelOne : MonoBehaviour
{
    [SerializeField] private AudioToPlay _audiosToPlay;

    [SerializeField] private AudioSource _audioSourceBackGround;

    [SerializeField] private AudioSource _audioSourceHouseBackGround;
    [SerializeField] private AudioSource _audioSourceWhenVaseHitsGround;
    [SerializeField] private AudioSource _audioSourceWhenPlayerPutGlassPieceInMirror;

    // [SerializeField] private AudioSource _audioSourceConversationAtStartLevel;

    // [SerializeField] private AudioSource _audioSourcePlayWhenPlayerEntersBathRoom;
    // [SerializeField] private AudioSource _audioSourceWhenPlayerGetsGlassPieceBathroom;
    // [SerializeField] private AudioSource _audioSourceWhenPlayerGetsGlassPieceBedroom;
    // [SerializeField] private AudioSource _audioSourceConversationWhenPlayerPutBathroomMirrorPiece;
    // [SerializeField] private AudioSource _audioSourceStarterConversationWhenPlayerGetsPicture;

    // [SerializeField] private AudioSource _audioSourceWhenPlayerTriggersFridge;
    // [SerializeField] private AudioSource _audioSourceWhenMirrorIsCompleted;
    // [SerializeField] private AudioSource _audioSourceWhenStartsVaseFlashback;
    // [SerializeField] private AudioSource _audioSourceWhenPlayerTriggerLockedDoorSound;


    private IEventManager _eventManager;
    private Timer _timer;

    private bool _firstAudioIsPlaying;

    private void Awake()
    {
        _eventManager = ServiceLocator.Instance.GetService<IEventManager>();
        _timer = new Timer();
        _eventManager.AddActionToSignal<PlayBackgroundMusicLevelOneSignal>(PlayBackGroundLevel);
        _eventManager.AddActionToSignal<DisableMovementAfterEventStartedSignal>(PlayConversationAtStartLevel);
        _eventManager.AddActionToSignal<PlayHouseBackGroundMusicLevelOneSignal>(PlayHouseBackGroundMusicLevelOne);
        _eventManager.AddActionToSignal<PlayAudioAfterPickingBathRoomLevelOneGlassSignal>(
            PlayAudioAfterPickingBathRoomGlass);
        _eventManager.AddActionToSignal<PlayAudioAfterPickingBedroomLevelOneGlassSignal>(
            PlayFirstAudioAfterPickingBedroomGlass);
        _eventManager.AddActionToSignal<PlayVaseHitsGroundSignal>(PlayAudioVaseHitsGround);
        _eventManager.AddActionToSignal<PlayAudioAfterPlayerTriggersFridgeSignal>(PlayAudioAfterTriggeredFridge);
        _eventManager.AddActionToSignal<PlayAudioLevelOneMirrorIsCompletedSignal>(PlayAudioMirrorIsCompleted);
        _eventManager.AddActionToSignal<PlaySoundFlashBackVaseSignal>(PlayAudioStartFlashBackVase);
        _eventManager.AddActionToSignal<PlayWhenPlayerEntersBathRoomSignal>(PlayWhenPlayerEntersBathRoom);
        _eventManager.AddActionToSignal<PlayAudioAfterTakingMotherPictureSignal>(PlayAudioAfterTakingPicture);
        _eventManager.AddActionToSignal<PlayConversationWhenPlayerHasPutFirstMirrorPieceSignal>(
            StartConversationWhenPlayerHasPutFirstMirrorPiece);
        _eventManager.AddActionToSignal<PlayGlassPiecesIsPutInMirrorAudioSignal>(PlayGlassPiecesIsPutInMirrorAudio);
        _eventManager.AddActionToSignal<PlayWhenTriggerLockedDoorSoundSignal>(PlayWhenTriggerLockedDoorSound);


        // _readInputPlayer.OnDebugEscPressed += PauseStartAudio;
    }


    private void OnDestroy()
    {
        _eventManager.RemoveActionFromSignal<PlayBackgroundMusicLevelOneSignal>(PlayBackGroundLevel);
        _eventManager.RemoveActionFromSignal<DisableMovementAfterEventStartedSignal>(PlayConversationAtStartLevel);
        _eventManager.RemoveActionFromSignal<PlayHouseBackGroundMusicLevelOneSignal>(PlayHouseBackGroundMusicLevelOne);
        _eventManager.RemoveActionFromSignal<PlayAudioAfterPickingBathRoomLevelOneGlassSignal>(
            PlayAudioAfterPickingBathRoomGlass);
        _eventManager.RemoveActionFromSignal<PlayAudioAfterPickingBedroomLevelOneGlassSignal>(
            PlayFirstAudioAfterPickingBedroomGlass);
        _eventManager.RemoveActionFromSignal<PlayVaseHitsGroundSignal>(PlayAudioVaseHitsGround);
        _eventManager.RemoveActionFromSignal<PlayAudioAfterPlayerTriggersFridgeSignal>(PlayAudioAfterTriggeredFridge);
        _eventManager.RemoveActionFromSignal<PlayAudioLevelOneMirrorIsCompletedSignal>(PlayAudioMirrorIsCompleted);
        _eventManager.RemoveActionFromSignal<PlaySoundFlashBackVaseSignal>(PlayAudioStartFlashBackVase);
        _eventManager.RemoveActionFromSignal<PlayWhenPlayerEntersBathRoomSignal>(PlayWhenPlayerEntersBathRoom);
        _eventManager.RemoveActionFromSignal<PlayAudioAfterTakingMotherPictureSignal>(PlayAudioAfterTakingPicture);
        _eventManager.RemoveActionFromSignal<PlayConversationWhenPlayerHasPutFirstMirrorPieceSignal>(
            StartConversationWhenPlayerHasPutFirstMirrorPiece);
        _eventManager
            .RemoveActionFromSignal<PlayGlassPiecesIsPutInMirrorAudioSignal>(PlayGlassPiecesIsPutInMirrorAudio);
        _eventManager.RemoveActionFromSignal<PlayWhenTriggerLockedDoorSoundSignal>(PlayWhenTriggerLockedDoorSound);
    }

    private void PlayWhenTriggerLockedDoorSound(PlayWhenTriggerLockedDoorSoundSignal obj)
    {
        var audioToPlay = _audiosToPlay.GetAudio("WhenPlayerTriggerLockedDoor",
            ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageKey());
        if (audioToPlay.isPlaying)
        {
            return;
        }

        audioToPlay.Play();
    }


    private void PlayGlassPiecesIsPutInMirrorAudio(PlayGlassPiecesIsPutInMirrorAudioSignal obj)
    {
        _audioSourceWhenPlayerPutGlassPieceInMirror.Play();
    }

    private void PauseStartAudio()
    {
        if (!_firstAudioIsPlaying)
        {
            return;
        }

        _firstAudioIsPlaying = false;
        _audiosToPlay.GetAudio("ConversationAtStartLevel",
            ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageKey()).Stop();

        new EnablePlayerMovementSignal().Execute();
    }

    private void PlayAudioStartFlashBackVase(PlaySoundFlashBackVaseSignal obj)
    {
        Debug.Log("Empieza FlashBackVase");
        _audiosToPlay.GetAudio("WhenStartsVaseFlashback",
            ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageKey()).Play();
    }

    private void PlayAudioMirrorIsCompleted(PlayAudioLevelOneMirrorIsCompletedSignal obj)
    {
        var audioToPlay = _audiosToPlay.GetAudio("WhenMirrorIsCompleted",
            ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageKey());
        audioToPlay.Play();
        _timer.OnTimerEnds += CheckAudioMirrorIsCompleted;
        _timer.SetTimeToWait(audioToPlay.clip.length);
        Debug.Log($"DURACION AUDIO {audioToPlay.clip.length} EMPEZAMOS en {Time.fixedTime}");
        StartCoroutine(_timer.TimerCoroutine());
    }

    private void CheckAudioMirrorIsCompleted()
    {
        Debug.Log($"TERMINAMOS  {Time.fixedTime}");
        _timer.OnTimerEnds -= CheckAudioMirrorIsCompleted;
        new AudioEndLevelOneHasEndedSignal().Execute();
    }

    private void PlayAudioAfterTriggeredFridge(PlayAudioAfterPlayerTriggersFridgeSignal obj)
    {
        Debug.Log("Empieza FlashBack Frigde");

        _audiosToPlay.GetAudio("WhenPlayerTriggersFridge",
            ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageKey()).Play();
    }

    private void PlayAudioVaseHitsGround(PlayVaseHitsGroundSignal obj)
    {
        _audioSourceWhenVaseHitsGround.Play();
    }


    private void PlayFirstAudioAfterPickingBedroomGlass(PlayAudioAfterPickingBedroomLevelOneGlassSignal obj)
    {
        _audiosToPlay.GetAudio("WhenPlayerGetsGlassPieceBedroom",
            ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageKey()).Play();
    }

    private void PlayAudioAfterPickingBathRoomGlass(PlayAudioAfterPickingBathRoomLevelOneGlassSignal obj)
    {
        Debug.Log("Empieza audio cristal baño");

        _audiosToPlay.GetAudio("WhenPlayerGetsGlassPieceBathroom",
            ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageKey()).Play();
    }

    private void PlayHouseBackGroundMusicLevelOne(PlayHouseBackGroundMusicLevelOneSignal obj)
    {
        _audioSourceHouseBackGround.Play();
    }

    private void PlayAudioAfterTakingPicture(PlayAudioAfterTakingMotherPictureSignal obj)
    {
        var audioToPlay = _audiosToPlay.GetAudio("WhenPlayerGetsPicture",
            ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageKey());
        audioToPlay.Play();
        Debug.Log("Empieza audio foto");
        _timer.OnTimerEnds += CallEnableGlassMirrorUnderTheBedSignal;
        _timer.SetTimeToWait(audioToPlay.clip.length);
        StartCoroutine(_timer.TimerCoroutine());
    }

    private void CallEnableGlassMirrorUnderTheBedSignal()
    {
        _timer.OnTimerEnds -= CallEnableGlassMirrorUnderTheBedSignal;
        new EnableGlassMirrorUnderTheBedSignal().Execute();
    }

    private void StartConversationWhenPlayerHasPutFirstMirrorPiece(
        PlayConversationWhenPlayerHasPutFirstMirrorPieceSignal obj)
    {
        Debug.Log("Empieza audio primer trozo cristal");

        _audiosToPlay.GetAudio("WhenPlayerPutBathroomMirrorPiece",
            ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageKey()).Play();
    }

    private void PlayWhenPlayerEntersBathRoom(PlayWhenPlayerEntersBathRoomSignal obj)
    {
        Debug.Log("SUena primer audio baño");
        _audiosToPlay.GetAudio("WhenPlayerEntersBathroom",
            ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageKey()).Play();
    }

    private void PlayBackGroundLevel(PlayBackgroundMusicLevelOneSignal signal)
    {
        Debug.Log("Suena musica de fondo");
        _audioSourceBackGround.Play();
        _audioSourceHouseBackGround.Play();
    }

    private void PlayConversationAtStartLevel(DisableMovementAfterEventStartedSignal obj)
    {
        _firstAudioIsPlaying = true;
        Debug.Log("PLAY 1 AUDIO CONVERSACION");
        var audioToPlay = _audiosToPlay.GetAudio("ConversationAtStartLevel",
            ServiceLocator.Instance.GetService<ILanguageManager>().GetActualLanguageKey());
        audioToPlay.Play();
        _timer.OnTimerEnds += EndStartConversationAtStartLevel;
        _timer.SetTimeToWait(audioToPlay.clip.length);
        StartCoroutine(_timer.TimerCoroutine());
    }

    private void EndStartConversationAtStartLevel()
    {
        Debug.Log("END AUDIO CONVERSACION");
        _timer.OnTimerEnds -= EndStartConversationAtStartLevel;
        _firstAudioIsPlaying = false;
        new EnablePlayerMovementSignal().Execute();
        new EnablePauseMenuSignal().Execute();

        new EnableMovementAfterEventEndedSignal().Execute();
    }
}