﻿using UnityEngine;

public class InitialActionsLevelZeroUseCase : MonoBehaviour
{
    [SerializeField] private Transform player, _originalPositionPlayer;
    void Start()
    {
        player.SetPositionAndRotation(_originalPositionPlayer.position, Quaternion.identity);;
        new DisablePlayerMovementSignal().Execute();
        new DisableMouseMovementSignal().Execute();
        new DisableMovementAfterEventStartedSignal().Execute();
    }
}