﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class GlassPieceManagerLevelOne : GlassPieceManager
{
    [SerializeField] private List<GlassInitInfo> _glassPieces;

    public override List<GlassInitInfo> GlassPieces()
    {
        return _glassPieces;
    }

    void Start()
    {
        base.Start();
        _eventManager.AddActionToSignal<PlayerGetsGlassPieceSignal>(PlayerGetsGlassPiece);

        _eventManager.AddActionToSignal<EnableRestOfGlassPiecesSignal>(EnableRestOfPieces);
        _eventManager.AddActionToSignal<EnableGlassMirrorUnderTheBedSignal>(EnableGlassMirrorUnderTheBed);
        foreach (var glassPiece in _glassPieces)
        {
            if (glassPiece.IsEnabledAtFirst)
            {
                _idGlassEnabledAtFirst = glassPiece.GlassPiece.PieceId;
            }

            glassPiece.GlassPiece.gameObject.SetActive(glassPiece.IsEnabledAtFirst);
        }
    }


    private void PlayerGetsGlassPiece(PlayerGetsGlassPieceSignal obj)
    {
        var glassInitInfo = _glassPieces.Single(x => x.GlassPiece.PieceId == obj.PieceOfGlass);

        if (glassInitInfo.Localization == "BathRoom")
        {
            new PlayAudioAfterPickingBathRoomLevelOneGlassSignal().Execute();
        }
        else if (glassInitInfo.Localization == "Bed")
        {
            new PlayAudioAfterPickingBedroomLevelOneGlassSignal().Execute();
        }
    }

    private void EnableGlassMirrorUnderTheBed(EnableGlassMirrorUnderTheBedSignal obj)
    {
        GlassInitInfo glassInitInfo =
            _glassPieces.Single(x => x.Localization.ToString() == "Bed");
        glassInitInfo.GlassPiece.gameObject.SetActive(true);
    }

    protected override void PlayerHasPutGlassPieceOnMirror(PlayerPutsGlassPieceOnMirrorSignal obj)
    {
        base.PlayerHasPutGlassPieceOnMirror(null);

        if (_glassPiecesRecovered == _glassPieces.Count)
        {
            new PlayAudioLevelOneMirrorIsCompletedSignal().Execute();
            new ShowFlashBackWhenMirrorIsCompletedLevelOneSignal().Execute();
            return;
        }

        GlassInitInfo glassInitInfo = _glassPieces.Single(x => x.GlassPiece.PieceId == obj.GlassPieceId);
        if (!glassInitInfo.IsEnabledAtFirst)
        {
            return;
        }

        PlayerHasPutBathGlassPieceInMirror();
    }

    private static void PlayerHasPutBathGlassPieceInMirror()
    {
        new PlayConversationWhenPlayerHasPutFirstMirrorPieceSignal().Execute();
        new EnableRestOfGlassPiecesSignal().Execute();
    }


    private void EnableRestOfPieces(EnableRestOfGlassPiecesSignal signal)
    {
        foreach (var glassPiece in _glassPieces)
        {
            if (glassPiece.GlassPiece.PieceId == _idGlassEnabledAtFirst)
            {
                continue;
            }

            if (glassPiece.Localization == "Bed")
            {
                continue;
            }

            glassPiece.GlassPiece.gameObject.SetActive(true);
        }
    }
}