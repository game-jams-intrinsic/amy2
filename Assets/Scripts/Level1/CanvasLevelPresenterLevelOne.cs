﻿using System.Linq;
using UnityEngine;

public class CanvasLevelPresenterLevelOne : CanvasLevelPresenter
{
    private FlashBackViewLevelOne _flashBackViewLevelOne;
    [SerializeField] private GlassPieceManager _actualGlassPieceManager;

    private void Awake()
    {
        base.Awake();
        _flashBackViewLevelOne = _flashBackViewLevel.GetComponent<FlashBackViewLevelOne>();
    }

    private void Start()
    {
        base.Start();

        _eventManager.AddActionToSignal<ShowVaseFlashBacksSignal>(ShowFlashBacksVase);
        _eventManager.AddActionToSignal<ShowFlashbacksFridgeSignal>(ShowFlashBacksFridge);
        _eventManager.AddActionToSignal<ShowFlashBackWhenMirrorIsCompletedLevelOneSignal>(
            ShowFlashBackWhenMirrorIsCompleted);
    }

    private void OnDestroy()
    {
        base.OnDestroy();
        _eventManager.RemoveActionFromSignal<ShowVaseFlashBacksSignal>(ShowFlashBacksVase);
        _eventManager.RemoveActionFromSignal<ShowFlashbacksFridgeSignal>(ShowFlashBacksFridge);
        _eventManager.RemoveActionFromSignal<ShowFlashBackWhenMirrorIsCompletedLevelOneSignal>(
            ShowFlashBackWhenMirrorIsCompleted);
    }

    protected override void ShowPiece(PlayerGetsGlassPieceSignal obj)
    {
        _pieceImage.gameObject.SetActive(true);
        var piece = _actualGlassPieceManager.GlassPieces().Single(x => x.GlassPiece.PieceId == obj.PieceOfGlass);
        _pieceImage.sprite = piece.SpriteToShow;
    }

    private void ShowFlashBackWhenMirrorIsCompleted(ShowFlashBackWhenMirrorIsCompletedLevelOneSignal obj)
    {
        new ShowFlashBackOrImagesSignal().Execute();
        new ShowOnlyUIMaskSignal().Execute();
        HideGlassPiece();
        _flashBackViewLevelOne.gameObject.SetActive(true);
        _flashBackViewLevelOne.ShowFlashWhenMirrorIsCompleted();
    }

    private void ShowFlashBacksFridge(ShowFlashbacksFridgeSignal obj)
    {
        new ShowFlashBackOrImagesSignal().Execute();

        new ShowOnlyUIMaskSignal().Execute();
        HideGlassPiece();
        _flashBackViewLevel.gameObject.SetActive(true);
        _flashBackViewLevelOne.ShowFlashBlackFridge();
    }

    private void ShowFlashBacksVase(ShowVaseFlashBacksSignal obj)
    {
        new ShowFlashBackOrImagesSignal().Execute();

        new ShowOnlyUIMaskSignal().Execute();
        HideGlassPiece();
        _flashBackViewLevelOne.gameObject.SetActive(true);
        _flashBackViewLevelOne.ShowFlashBlackVase();
    }
}