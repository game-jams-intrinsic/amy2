﻿using App.PlayerModelInfo;
using Domain.JsonTranslator;
using Domain.LoaderSaveGame;
using Domain.SaverGame;
using Presentation.Player;
using Signals.EventManager;
using UnityEngine;
using Utils;
using Utils.Input;

namespace Presentation.Installers
{
    public class InstallerInGamePresentation : MonoBehaviour
    {
        private GameObject _readInputPlayerPrefab,
            _languageManagerPrefab,
            _sceneChangerManagerPrefab, _updatePlayerProgressionPrefab;

        private GameObject _readInputPlayerInstance,
            _languageManagerInstance,
            _sceneChangerManagerInstance, _updatePlayerProgressionInstance;

        private void Awake()
        {
            _readInputPlayerPrefab = Resources.Load("Prefabs/Common/Managers/ReadInputPlayer") as GameObject;
            _languageManagerPrefab = Resources.Load("Prefabs/Common/Managers/LanguageManager") as GameObject;
            _sceneChangerManagerPrefab = Resources.Load("Prefabs/Common/Managers/SceneChangerManager") as GameObject;
            _updatePlayerProgressionPrefab = Resources.Load("Prefabs/Common/Managers/UpdatePlayerProgression") as GameObject;

            ServiceLocator.Instance.RegisterModel<IPlayerModel>(new PlayerModel());
            ServiceLocator.Instance.RegisterModel<IEndLevelModel>(new EndLevelModel());
            
            ServiceLocator.Instance.RegisterService<IEventManager>(new EventManager());
            ServiceLocator.Instance.RegisterService<IJsonator>(new JsonUtililyTransformer());
            ServiceLocator.Instance.RegisterService<ISaver>(new SaveUsingPlayerPrefs());
            ServiceLocator.Instance.RegisterService<ILoader>(new LoadWithPlayerPrefs());
            
            _readInputPlayerInstance = Instantiate(_readInputPlayerPrefab, transform);
            _languageManagerInstance = Instantiate(_languageManagerPrefab, transform);
            _sceneChangerManagerInstance = Instantiate(_sceneChangerManagerPrefab, transform);
            _updatePlayerProgressionInstance = Instantiate(_updatePlayerProgressionPrefab, transform);

            ServiceLocator.Instance.RegisterService(_languageManagerInstance.GetComponent<ILanguageManager>());

            ServiceLocator.Instance.RegisterService(_readInputPlayerInstance.GetComponent<ReadInputPlayer>());
            ServiceLocator.Instance.RegisterService(_sceneChangerManagerInstance.GetComponent<SceneChanger>());

            // DontDestroyOnLoad(this);
        }

        private void OnDestroy()
        {
            ServiceLocator.Instance.UnregisterService<ReadInputPlayer>();
            ServiceLocator.Instance.UnregisterService<IEventManager>();
            ServiceLocator.Instance.UnregisterService<ILanguageManager>();
        }
    }
}