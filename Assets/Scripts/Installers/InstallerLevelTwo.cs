﻿using UnityEngine;
using Utils;

namespace Presentation.Installers
{
    public class InstallerLevelTwo : MonoBehaviour
    {
        private void Awake()
        {
            ServiceLocator.Instance.RegisterModel<IConsolePanelModel>(new ConsolePanelModel());
        }
    }
}