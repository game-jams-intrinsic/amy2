﻿using Signals;
using UnityEngine;

public class ShowTextOnCanvasSignal : SignalBase
{
    public string TextToShow;
    public Color Color;
}