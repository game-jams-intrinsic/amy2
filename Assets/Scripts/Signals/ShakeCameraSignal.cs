﻿using Signals;

public class ShakeCameraSignal :SignalBase
{
    public float TimeToShake;
    public ActionToDo ActionToDoAfter;
}