﻿using Signals;
using UnityEngine;

public class ShowPictureOnCanvasSignal : SignalBase
{
    public Sprite PictureToShow;
}