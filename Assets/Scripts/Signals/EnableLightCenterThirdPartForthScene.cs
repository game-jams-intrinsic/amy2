﻿using UnityEngine;

[CreateAssetMenu(fileName = "EnableThirdLight", menuName = "ActionsToDo/EnableThirdLight")]
public class EnableLightCenterThirdPartForthScene : ActionToDo
{
    public override void ExecuteAction()
    {
        new EnableLightCenterThirdPartSignal().Execute();
        new DisableReturnToSecondPartSignal().Execute();
        new DisableMirrorsLimboSignal().Execute();
    }
}
