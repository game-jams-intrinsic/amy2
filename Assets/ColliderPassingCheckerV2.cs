﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderPassingCheckerV2 : ColliderPassingChecker
{
    public override bool HasBeenCrossedCompletelyByPlayer()
    {
        if (_entranceAxis == EntranceAxis.HORIZONTAL)
        {
            return Mathf.Abs(_positionOfPlayerAtStartEntrance.z - _positionOfPlayerAtExitEntrance.z) > _collider.bounds.extents.z;
        }

        Debug.Log($"{_positionOfPlayerAtStartEntrance.x - _positionOfPlayerAtExitEntrance.x} SIZE {_sizeCollider.x}");
        bool hasCrossed = Mathf.Abs(_positionOfPlayerAtStartEntrance.x - _positionOfPlayerAtExitEntrance.x) > _collider.bounds.extents.x;
        return hasCrossed;
    }
    
}